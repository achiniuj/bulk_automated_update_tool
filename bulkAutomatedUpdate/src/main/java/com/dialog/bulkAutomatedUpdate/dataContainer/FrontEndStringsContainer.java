package com.dialog.bulkAutomatedUpdate.dataContainer;

public class FrontEndStringsContainer {

		public enum StringsAddressChangePage{
			
			/** The table address detail header, Default Address. */
			TABLE_ADDRES_HEADER_DEFAULT_ADDRESS("Default Address"),
			
			/** The table address detail header,Address Id. */
			TABLE_ADDRES_HEADER_ADDRESS_ID("Address Id"),
			
			/** The table address detail header,AddressType. */
			TABLE_ADDRES_HEADER_ADDRESS_TYPE("AddressType"),
			
			/** The table address detail header,Line 1. */
			TABLE_ADDRES_HEADER_LINE1("Line 1"),
			
			/** The table address detail header,Line 2. */
			TABLE_ADDRES_HEADER_LINE2("Line 2"),
			
			/** The table address detail header,Email Address. */
			TABLE_ADDRES_HEADER_EMAIL("Email Address"),
			
			/** The address type, off office address. */
			ADDRESS_TYPE_OFF_OFFICE("off office address"),
			
			/** The address successful update pop message. */
			UPDATE_ADDRESS_SUCCESS_MSG("Data saved"),
			
			/**The Address Created Message*/
			ADDRESS_CREATED_SUCCESS_MSG("Address Created");
			
			/**  The value. */
			private String value;
			
			/**
			 * Instantiates a new strings address change page.
			 *
			 * @author AchiniUJ
			 * @param value the value
			 */
			StringsAddressChangePage(String value) {
				this.value = value;
			}
			
			/**
			 * Value.
			 *
			 * @author AchiniUJ
			 * @return the string
			 */
			public String value() {
				return value;
			}
		}
		
		public enum StringsSubscriberProfileUpdatePage{
			
			/** The update success popup msg. */
			UPDATE_SUCCESS_POPUP_MSG("Profile Updated Successfully");
			
			/**  The value. */
			private String value;
			
			/**
			 * Instantiates a new strings subscriber profile update page.
			 *
			 * @author AchiniUJ
			 * @param value the value
			 */
			StringsSubscriberProfileUpdatePage(String value) {
				this.value = value;
			}

			/**
			 * Value.
			 *
			 * @author AchiniUJ
			 * @return the string
			 */
			public String value() {
				return value;
			}
		}
		
		public enum StringsAccountUpdatePage{

			/**The Account Update Success Message*/
			ACCOUNT_UPDATE_SUCCESS_MSG("No Hierarchy change to save"); //Not a Clear Message
			
			/**  The value. */
			private String value;
			
			/**
			 * Instantiates a new strings Account Update Page.
			 *
			 * @author MalshaniS
			 * @param value the value
			 */
			StringsAccountUpdatePage(String value) {
				this.value = value;
			}

			/**
			 * Value.
			 *
			 * @author MalshaniS
			 * @return the string
			 */
			public String value() {
				return value;
			}
		}
	
		public enum StringsContractUpdatePage{

			/**The Attribute Change Success Message*/
			ATTRIBUTE_CHANGE_SUCCESS_MSG("Changes Saved:Valid Change"); 
			
			/**  The value. */
			private String value;
			
			/**
			 * Instantiates a new strings Contract Update Page.
			 *
			 * @author MalshaniS
			 * @param value the value
			 */
			StringsContractUpdatePage(String value) {
				this.value = value;
			}

			/**
			 * Value.
			 *
			 * @author MalshaniS
			 * @return the string
			 */
			public String value() {
				return value;
			}
		}	   
		
		public enum StringsEbillConfirmationPage{

			/** The email successful add pop message. */
			ADD_NEW_EMAIL_SUCCESS_MSG("Data saved"),
			
			/** The email successful Updated pop message. */
			EMAIL_UPDATE_SUCCESS_MSG("Successfully Added.");
			
			/**  The value. */
			private String value;
			
			/**
			 * Instantiates a new Strings Ebill Confirmation Page.
			 *
			 * @author MalshaniS
			 * @param value the value
			 */
			StringsEbillConfirmationPage(String value) {
				this.value = value;
			}

			/**
			 * Value.
			 *
			 * @author MalshaniS
			 * @return the string
			 */
			public String value() {
				return value;
			}
		}	
		
		public enum StringsPackageChangePage{

			/** The GSM Package Change Successful Message. */
			GSM_PACKAGE_CHANGE_SUCCESS_MSG("GSM Package Change Successfull");
			
			/**  The value. */
			private String value;
			
			/**
			 * Instantiates a new strings Package Change Page.
			 *
			 * @author MalshaniS
			 * @param value the value
			 */
			StringsPackageChangePage(String value) {
				this.value = value;
			}

			/**
			 * Value.
			 *
			 * @author MalshaniS
			 * @return the string
			 */
			public String value() {
				return value;
			}
		}	 
		
		public enum StringsCxRegistrationPage{

			CX_REG_SUCCESS_MSG_POPUP(" : SUCCESS");
				
			/**  The value. */
			private String value;
			
			/**
			 * Instantiates a new strings Cx Registration Page.
			 *
			 * @author MalshaniS
			 * @param value the value
			 */
			StringsCxRegistrationPage(String value) {
				this.value = value;
			}

			/**
			 * Value.
			 *
			 * @author MalshaniS
			 * @return the string
			 */
			public String value() {
				return value;
			}
		}	
		
}
