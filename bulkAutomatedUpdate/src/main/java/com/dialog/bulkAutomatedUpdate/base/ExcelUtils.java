package com.dialog.bulkAutomatedUpdate.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * The Class ExcelUtils
 *
 */
public class ExcelUtils {
	
	/**The workbook*/
	public static XSSFWorkbook workbook;
	
	/**The workbookSheet*/
	public static XSSFSheet workbookSheet;
	
	/**The cell*/
	public static Cell cell;
	
	/**The row*/
	public static XSSFRow row;
	
	/**The file*/
	public static FileInputStream file;
	
	/** The file out. */
	public static FileOutputStream fileOut;
	
	/**The cell Style*/
	public static XSSFCellStyle cellStyle;
	
	/**
	 * Set the file path and Open Excel File
	 * @author MalshaniS
	 * @return 
	 *
	 * @throws IOException
	 */
	public void readExcelFile(String filePath, String sheetName) throws IOException{
		try{
		
			file = new FileInputStream(new File(filePath));
		
			/**Get the workbook instance for XLS file*/
			workbook = new XSSFWorkbook(file);
			
			workbookSheet = workbook.getSheet(sheetName);
			changeActiveSheet(sheetName);
				
			row = workbookSheet.getRow(1);
			
		}catch(Exception e){
			e.printStackTrace();
			throw(e);
		}
	}
	
	/**
	 * Get Row Count of the Active Sheet
	 * @author MalshaniS
	 *
	 * @return
	 */
	public int getSheetRowCount(){
		int rowCount = workbookSheet.getLastRowNum() + 1;
		return rowCount;
	}

	/**
	 * Method to Assign Header Sequence Number
	 * @author MalshaniS
	 *
	 * @param listHeader
	 * @return 
	 */
    public Map<Integer, String> assignHeaderSequenceNumber(List<String> listHeader){
    	
        Map<Integer, String> headerSequenceNumber = new HashMap<Integer, String>();

        int i=0;
        for (String columnName : listHeader) {
        	headerSequenceNumber.put(i, columnName);
              i++;
        }

        return headerSequenceNumber;
    }
    
    /**
     * Check Sheet exist
     * @author MalshaniS
     *
     * @param sheetIndex
     * @return true, if sheet exist
     */
	public boolean isSheetExist(String sheetName) {
		int sheetIndex = workbook.getSheetIndex(sheetName);
		if (sheetIndex == -1){
			sheetIndex = workbook.getSheetIndex(sheetName.toUpperCase());
			if (sheetIndex == -1)
				return false;
			else
				return true;
		}
		else
			return true;
	}
    
	/**
	 * Get Column Count
	 * @author MalshaniS
	 *
	 * @param sheetIndex
	 * @return column count
	 */
	public int getColumnCount(String sheetName) {
		if(!isSheetExist(sheetName)){
			return -1;
		}
		workbookSheet = workbook.getSheet(sheetName);
		row = workbookSheet.getRow(0);
		
		if (row == null){
			return -1;
		}
		return row.getLastCellNum();
		
	}
    
	/**Will find all the cells on the row 1
	 * 
	 * @param sheetIndex
	 * @return Array of string with the header row labels.
	 */
	public List<String> getHeaderRowValues(String sheetName){
		int columnCount = getColumnCount(sheetName);
		
		List<String> header = new ArrayList<>();
		
		for (int i=0; i< columnCount; i++){
			header.add(getHeaderCellData(i, 1));
		}
		return header;
	}
	
	/**
	 * Get Header Cell Data
	 *
	 * @param columnNum
	 * @param rowNum
	 * @return Header cell Text
	 */
	private String getHeaderCellData(int columnNum, int rowNum) {

		try {
			if (rowNum <= 0)
				return "";

			row = workbookSheet.getRow(rowNum - 1);
			if (row == null)
				return "";
			cell = row.getCell(columnNum);
			if (cell == null)
				return "";
			
			cell.setCellType(Cell.CELL_TYPE_STRING);
			
			return cell.getStringCellValue().trim();
			
		} catch (Exception e) {

			e.printStackTrace();
			return "row " + rowNum + " or column " + columnNum + " does not exist  in xls";
		}
	}
	
	/**
	 * Get Cell Data
	 *
	 * @param sheetIndex
	 * @param columnName
	 * @param rowNum
	 * @return cellText
	 */
	public String getCellData(String sheetName, String columnName, int rowNum) {
		if (columnName == null){
			return "Column Name is Null, hence cannot compare the cell";
		}
		try {
			if (rowNum <= 0)
				return "";
			
			int sheetIndex = workbook.getSheetIndex(sheetName);
			
			int columnNumber = -1;
			if (sheetIndex == -1)
				return "";

			workbookSheet = workbook.getSheetAt(sheetIndex);
			row = workbookSheet.getRow(0);
			for (int i = 0; i < row.getLastCellNum(); i++) {
				try {
					row.getCell(i).getStringCellValue();
					
				} catch (NullPointerException e){
					continue;
				}
				if (row.getCell(i).getStringCellValue().trim().equals(columnName.trim())){
					columnNumber = i;
					break;
				}
			}
			if (columnNumber == -1)
				return "";

			workbookSheet = workbook.getSheetAt(sheetIndex);
			row = workbookSheet.getRow(rowNum);
			
			if (row == null)
				return "";
			
			cell = row.getCell(columnNumber);

			if (cell == null)
				return "";
			
			if (cell.getCellType() == Cell.CELL_TYPE_STRING)
				return cell.getStringCellValue();
			
			else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC || cell.getCellType() == Cell.CELL_TYPE_FORMULA) {
				String cellText = String.valueOf((int) cell.getNumericCellValue());
				
				if (HSSFDateUtil.isCellDateFormatted(cell)) {
					double d = cell.getNumericCellValue();

					Calendar cal = Calendar.getInstance();
					cal.setTime(HSSFDateUtil.getJavaDate(d));
					cellText = (String.valueOf(cal.get(Calendar.YEAR))).substring(2);
					cellText = cal.get(Calendar.DAY_OF_MONTH) + "/" + cal.get(Calendar.MONTH) + 1 + "/"
							+ cellText;
				}

				return cellText;
				
			} else if (cell.getCellType() == Cell.CELL_TYPE_BLANK)
				return "";
			else
				return String.valueOf(cell.getBooleanCellValue());

		} catch (Exception e) {

			e.printStackTrace();
			return "row " + rowNum + " or column " + columnName + " does not exist in xls";
		}
	}
	
	/**
	 * Gets the cell data.
	 * 
	 * @param sheetIndex
	 *            the sheet index
	 * @param colNum
	 *            the col num
	 * @param rowNum
	 *            the row num
	 * @return the cell data
	 */
	public String getCellData(String sheetName, int columnNum, int rowNum) {
		try {
			if (rowNum < 0)
				return "";
			
			int sheetIndex = workbook.getSheetIndex(sheetName);

			if (sheetIndex == -1)
				return "";

			workbookSheet = workbook.getSheetAt(sheetIndex);
			row = workbookSheet.getRow(rowNum);
			
			if (row == null)
				return "";
			
			cell = row.getCell(columnNum);
			
			if (cell == null)
				return "";

			if (cell.getCellType() == Cell.CELL_TYPE_STRING)
				return cell.getStringCellValue();
			
			else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC || cell.getCellType() == Cell.CELL_TYPE_FORMULA) {

				String cellText = String.valueOf(cell.getNumericCellValue());
				if (HSSFDateUtil.isCellDateFormatted(cell)) {

					double d = cell.getNumericCellValue();

					Calendar cal = Calendar.getInstance();
					cal.setTime(HSSFDateUtil.getJavaDate(d));
					cellText = (String.valueOf(cal.get(Calendar.YEAR))).substring(2);
					cellText = cal.get(Calendar.MONTH) + 1 + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/"+ cellText;
					
				}

				return cellText;
				
			} else if (cell.getCellType() == Cell.CELL_TYPE_BLANK)
				return "";
			else
				return String.valueOf(cell.getBooleanCellValue());
		} catch (Exception e) {

			e.printStackTrace();
			return "row " + rowNum + " or column " + columnNum + " does not exist  in xls";
		}
	}
	
	/**
	 * Set cell Data
	 *
	 * @param sheetIndex
	 * @param columnName
	 * @param rowNum
	 * @param data
	 * @return true, if cell output action complete
	 */
	public boolean setCellData(String filePath, String sheetName, String columnName, int rowNum, String data) {
		try {
			if (rowNum <= 0)
				return false;
			
			int sheetIndex = workbook.getSheetIndex(sheetName);

			int colNum = -1;
			if (sheetIndex == -1)
				return false;

			workbookSheet = workbook.getSheetAt(sheetIndex);

			row = workbookSheet.getRow(0);
			for (int i = 0; i < row.getLastCellNum(); i++) {
				try {
					row.getCell(i).getStringCellValue();
					
				} catch (NullPointerException e){
					continue;
				}
				if (row.getCell(i).getStringCellValue().trim().equals(columnName)){
					colNum = i;
					break;
				}
			}
			if (colNum == -1)
				return false;

			workbookSheet.autoSizeColumn(colNum);
			row = workbookSheet.getRow(rowNum);
			if (row == null){
				row = workbookSheet.createRow(rowNum - 1);
			}
			cell = row.getCell(colNum);
			if (cell == null){
				cell = row.createCell(colNum);
			}

			//Output Cell Value
			cell.setCellValue(data);
			
			if(columnName == "Comment"){
			
				//Set cell colour
				cellStyle = workbook.createCellStyle(); 	
				cellStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
				if(data == "Updated Successfully"){
					cellStyle.setFillForegroundColor(IndexedColors.BRIGHT_GREEN.getIndex());
				}
				else{
					cellStyle.setFillForegroundColor(IndexedColors.GOLD.getIndex());
				}
						
				cell.setCellStyle(cellStyle);  
			}
			
			fileOut = new FileOutputStream(filePath);
			workbook.write(fileOut);
			fileOut.close();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
			
		}
		return true;
	}
	
	/**
	 * Change Cell Colour with Incorrect Data
	 * @author MalshaniS
	 *
	 * @param sheetIndex
	 * @param columnName
	 * @param rowNum
	 * @return
	 * @throws IOException
	 */
    public boolean highlightCellwithIncorrectData(String filePath, String sheetName, String columnName, int rowNum) throws IOException{
    	
		try {
			int columnNumber = 0;
			
			workbookSheet = workbook.getSheet(sheetName);
			row = workbookSheet.getRow(0);
			
			for (int i = 0; i < row.getLastCellNum(); i++) {
				try {
					row.getCell(i).getStringCellValue();
					
				} catch (NullPointerException e){
					continue;
				}
				if (row.getCell(i).getStringCellValue().trim().equals(columnName.trim())){
					columnNumber = i;
					break;
				}
			}
			
			cellStyle = workbook.createCellStyle(); 	
			cellStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
			cellStyle.setFillForegroundColor(IndexedColors.GOLD.getIndex());
			
			row = workbookSheet.getRow(rowNum);
			cell = row.getCell(columnNumber);
			
			cell.setCellStyle(cellStyle);   	
			fileOut = new FileOutputStream(new File(filePath));
	    	workbook.write(fileOut);
	    	fileOut.close();
	    	
		} catch (Exception e) {
			e.printStackTrace();
			return false;
			
		}
		return true;
    }
    
    /**
     * Get Cell Type
     * @author MalshaniS
     *
     * @param columnName
     * @param rowNum
     * @param sheetName
     * @return
     */
    public int getCellType(String columnName, int rowNum, String sheetName){
	
			int columnNumber = 0;

			workbookSheet = workbook.getSheet(sheetName);
			row = workbookSheet.getRow(0);
			
			for (int i = 0; i < row.getLastCellNum(); i++) {
				try {
					row.getCell(i).getStringCellValue();
					
				} catch (NullPointerException e){
					continue;
				}
				if (row.getCell(i).getStringCellValue().trim().equals(columnName.trim())){
					columnNumber = i;
					break;
				}
			}
			
			workbookSheet = workbook.getSheet(sheetName);
			row = workbookSheet.getRow(rowNum);
			
			cell = row.getCell(columnNumber);
			int cellType = cell.getCellType();
			return cellType;
    }
    
	/**Active sheet would be changed to the given sheet, and set the static variable to active. 
	 * 
	 * @param sheetName
	 */
	public void changeActiveSheet(String sheetName){
		int sheetIndex = workbook.getSheetIndex(sheetName);
		workbook.setActiveSheet(sheetIndex);
		workbookSheet = workbook.getSheetAt(sheetIndex);
	}
	
	/**
	 * Gets the row count active sheet.
	 * 
	 * @return the row count active sheet
	 */

	public int getRowCountActiveSheet() {
		int number = workbookSheet.getLastRowNum() + 1;
		return number;
	}
	
	/**
	 * 
	 * FInd Row
	 *
	 * @param sheetName
	 * @param cellContent
	 * @return row
	 */
	public int findRow(String sheetName, String cellContent) {
		int sheetIndex = workbook.getSheetIndex(sheetName);
		workbook.setActiveSheet(sheetIndex);
		workbookSheet = workbook.getSheetAt(sheetIndex);
	    for (Row row : workbookSheet) {
	        for (Cell cell : row) {
	            if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
	                if (cell.getRichStringCellValue().getString().trim().equals(cellContent)) {
	                    return row.getRowNum();  
	                }
	            }
	        }
	    }               
	    return 0;
	}
	
	/**
	 * Adds the column.
	 *
	 * @param filePath
	 * @param sheetName
	 * @param colName
	 * @return true, if successfull
	 */
	public boolean addColumn(String filePath, String sheetName, String colName) {
		try {
			int index = workbook.getSheetIndex(sheetName);
			if (index == -1){
				return false;
			}

			workbookSheet = workbook.getSheetAt(index);

			row = workbookSheet.getRow(0);
			if (row == null)
				row = workbookSheet.createRow(0);

			if (row.getLastCellNum() == -1)
				cell = row.createCell(0);
			else
				cell = row.createCell(row.getLastCellNum());

			cell.setCellValue(colName);

			fileOut = new FileOutputStream(new File(filePath));
			workbook.write(fileOut);
			fileOut.close();

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * 
	 * Get Last Column Number
	 *
	 * @param sheetName
	 * @return
	 */
	public int getLastColumnNumber(String sheetName) {
		// check if sheet exists
		if (!isSheetExist(sheetName))
			return -1;
		workbookSheet = workbook.getSheet(sheetName);
		row = workbookSheet.getRow(0);
		if (row == null)
			return -1;
		return (row.getLastCellNum()-1);
	}
	
	/**
	 * Set Cell Data Using Column and Row Number
	 * 
	 * @param filePath
	 * @param sheetName
	 * @param columnNo
	 * @param rowNum
	 * @param data
	 * @return
	 */
	public boolean setCellDataUsingColumnRowNumber(String filePath, String sheetName, int columnNo, int rowNum, String data){
		try {
			if (rowNum <= 0)
				return false;
			
			int sheetIndex = workbook.getSheetIndex(sheetName);

			workbookSheet = workbook.getSheetAt(sheetIndex);

			row = workbookSheet.getRow(0);

			if (columnNo == -1)
				return false;

			workbookSheet.autoSizeColumn(columnNo);
			row = workbookSheet.getRow(rowNum);
			if (row == null){
				row = workbookSheet.createRow(rowNum - 1);
			}
			cell = row.getCell(columnNo);
			if (cell == null){
				cell = row.createCell(columnNo);
			}

			//Output Cell Value
			cell.setCellValue(data);
				
			fileOut = new FileOutputStream(filePath);
			workbook.write(fileOut);
			fileOut.close();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
			
		}
		return true;
	}
}