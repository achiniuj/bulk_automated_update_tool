package com.dialog.bulkAutomatedUpdate.base;

import java.io.IOException;
import java.util.Hashtable;

import org.testng.ITestNGMethod;
import org.testng.annotations.DataProvider;


public class ExcelDataProvider {
	
	public static ExcelUtils excelFileReader;
	
	/**Get File Path*/
	public static ReadPropertyFileData propertyFile;
	
	@DataProvider(name="testData")
	public static Object[][] bulkUpdateScenario(ITestNGMethod context) throws Exception {
		Object[][] data = null;

		propertyFile = new ReadPropertyFileData();
		String filepath = propertyFile.getPropertyValue("dataFilePath");
		String sheetName = propertyFile.getPropertyValue("sheetName");		

		data = accessExcelFile(filepath, sheetName);
//		excelFileReader.addColumn(filepath, sheetName, "Flag");
		return data;
	}

	/**
	 * Call method.
	 *
	 * @param filepath the filepath
	 * @param SheetName the sheet name
	 * @return the object[][]
	 * @throws IOException 
	 * @throws Exception the exception
	 */
	public static Object[][] accessExcelFile(String filePath, String sheetName) throws IOException {
		Object[][] data = null;
		
		excelFileReader = new ExcelUtils();
		
		excelFileReader.readExcelFile(filePath, sheetName);
		
		Hashtable<String, String> table = null;
		int columnNo = 0;
		
		while (!excelFileReader.getCellData(sheetName, columnNo, 0).equals("")) {

			columnNo++;
		}
		columnNo = columnNo - 1;
		
		int rowNo = 0;
		while (!excelFileReader.getCellData(sheetName, 0, rowNo + 1).equals("")) {
			rowNo++;
		}
		data = new Object[rowNo][1]; 
		
		for (int row = 1; row < rowNo + 1; row++) {
			table = new Hashtable<String, String>();
			for (int col = 0; col <= columnNo; col++) {

				table.put(excelFileReader.getCellData(sheetName, col, 0), excelFileReader.getCellData(sheetName, col, row));
			}
			data[row - 1][0] = table;
		}
		return data;
	}
}
