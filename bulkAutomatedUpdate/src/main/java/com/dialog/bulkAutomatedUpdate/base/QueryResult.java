package com.dialog.bulkAutomatedUpdate.base;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;

/**
 * The Class QueryResult
 * 
 * @author MalshaniS
 *
 */
public class QueryResult {
	
	/** The result. */
	private List<Map<String, String>> result = new ArrayList<>();
	
	/**
	 * Get the value
	 *
	 * @param label
	 * @param index - starting from 0
	 * @return value
	 */
	public String getValue(String label, int index) {
		if (result.size() <= index) {
			return null;
		}
		Map<String, String> row = result.get(index);
		return row.get(label);
	}

	/**
	 * Gets the values.
	 * 
	 * @param label
	 * @return the values
	 */
	public List<String> getAllValues(String label) {
		label = label.toUpperCase();
		List<String> list = new ArrayList<>();
		if (result.isEmpty()) {
			return list;
		}
		for (Iterator<Map<String, String>> iterator = result.iterator(); iterator.hasNext();) {
			Map<String, String> map = (Map<String, String>) iterator.next();
			list.add(map.get(label));
		}

		return list;
	}

	public String getValueFromCondition(String selectField, String fromField, String matchCondition) {
		for (Iterator<Map<String, String>> iterator = result.iterator(); iterator.hasNext();) {
			Map<String, String> map = (Map<String, String>) iterator.next();
			if (matchCondition.equals(map.get(fromField))) {
				return map.get(selectField);
			}
		}
		throw new ElementNotFoundException(selectField, "", "");
	}

	/**
	 * Gets the value from first row.
	 * 
	 * @param label
	 * @return the value from first row
	 */
	public String getValueFromFirstRow(String label) {
		if (result.isEmpty()) {
			return null;
		}
		Map<String, String> row = result.get(0);
		if (row.get(label) == null) {
			return "";
		}
		return row.get(label);
	}
	
	/**
	 * Gets the row.
	 * 
	 * @param index
	 *            the index
	 * @return the row
	 */
	public Map<String, String> getRow(int index) {
		if (result.size() <= index) {
			return null;
		}
		Map<String, String> row = result.get(index);
		return row;
	}
	
	/**
	 * Gets the result size.
	 * 
	 * @return the result size
	 */
	public int getResultSize() {
		return result.size();
	}
	
	/**
	 * Enque row.
	 * 
	 * @param row
	 *            the row
	 */
	public void enqueueRow(Map<String, String> row) {
		result.add(row);
	}
}
