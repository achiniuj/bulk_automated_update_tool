package com.dialog.bulkAutomatedUpdate.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

public class ReadPropertyFileData {

	public String getPropertyValue(String propName) throws IOException{	
		File propertyFile=new File("C:/bulkAutomatedUpdate/dataFile.properties");
		FileInputStream fileInput = null;
		fileInput = new FileInputStream(propertyFile);
		Properties prop=new Properties();
		prop.load(fileInput);
		return prop.getProperty(propName);
	}
	
	public void setPropertyValue(String propName, String propValue) throws IOException, ConfigurationException{
		File propertyFile = new File("C:/bulkAutomatedUpdate/dataFile.properties");
		PropertiesConfiguration propConfig = new PropertiesConfiguration(propertyFile);
		propConfig.setProperty(propName, propValue);
		propConfig.save();
	}
}
