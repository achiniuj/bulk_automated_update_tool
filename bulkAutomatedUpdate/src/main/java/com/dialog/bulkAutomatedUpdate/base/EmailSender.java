package com.dialog.bulkAutomatedUpdate.base;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


public class EmailSender {

	public ReadPropertyFileData propFile = new ReadPropertyFileData();
	
	public String senderName;
	public String password;
	public String sendTo;
	public String copyTo;
	public String bccTo;
	public String subject;
	public String emailBody;
	public String scenarioName;
	public String proxyHost;
	public String proxyPort;
	public String proxyUser;
	public String proxyPassword;
	private static String protocol;
	
	private Session session;
    private static String sslTrust;
    
    private void setSslTrust(String sslTrust){
    	this.sslTrust = sslTrust;
    } 
	
	public EmailSender() throws IOException{
		senderName = propFile.getPropertyValue("email.sender");
		password = propFile.getPropertyValue("email.password");
		sendTo = propFile.getPropertyValue("email.sendTo");
		copyTo = propFile.getPropertyValue("email.sendCC");
		bccTo = propFile.getPropertyValue("email.sendBCC");
		subject = propFile.getPropertyValue("email.Subject");
		emailBody = propFile.getPropertyValue("email.Body");
		protocol = propFile.getPropertyValue("email.protocol");
		scenarioName = propFile.getPropertyValue("sheetName");
		
        if (propFile.getPropertyValue("proxy.enable").equalsIgnoreCase("true")){
        	proxyHost = propFile.getPropertyValue("proxy.host");
        	proxyPort = propFile.getPropertyValue("proxy.port");
        	proxyUser = propFile.getPropertyValue("proxy.user");
        	proxyPassword = propFile.getPropertyValue("proxy.password");
        }
	}
	
	/**
	 * Send an Email with Results
	 * @author MalshaniS
	 *
	 * @throws MessagingException
	 * @throws IOException 
	 */
	public void sendEmail() throws MessagingException, IOException{
		
		if(propFile.getPropertyValue("configure.email").equalsIgnoreCase("true")){
			if (propFile.getPropertyValue("proxy.enable").equalsIgnoreCase("true")){
				session = getSessionProxy();
			}
			else{
				session = getSession();
			}
		
			try{
			
				MimeMessage message = new MimeMessage(session);
				message.setFrom(new InternetAddress(senderName));
				message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(sendTo));
			
				if(!copyTo.isEmpty()){
					message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(copyTo));
				}
				if(!bccTo.isEmpty()){
					message.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(bccTo));
				}
			
				//Email Subject
				message.setSubject(subject + scenarioName + " - "+ getSystemDate());
			
				//Email Body - Content Text
				BodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setText(emailBody);		
				Multipart multiPart = new MimeMultipart();
				multiPart.addBodyPart(messageBodyPart);
			
				//Email Body - Data File Attachment
				messageBodyPart = new MimeBodyPart();
				String dataFilePath = propFile.getPropertyValue("dataFilePath");		
				DataSource sourceDateFile = new FileDataSource(dataFilePath);
				messageBodyPart.setDataHandler(new DataHandler(sourceDateFile));
				messageBodyPart.setFileName("DataFile_"+scenarioName+"_"+getSystemDate()+".xlsx");
				multiPart.addBodyPart(messageBodyPart);
				
				//Email Body - Report File Attachment
				messageBodyPart = new MimeBodyPart();
				String reportFilePath = propFile.getPropertyValue("reportFilePath");			
				DataSource sourceReportFile = new FileDataSource(reportFilePath);
				messageBodyPart.setDataHandler(new DataHandler(sourceReportFile));
				messageBodyPart.setFileName("ReportFile_"+scenarioName+"_"+getSystemDate()+".html");
				multiPart.addBodyPart(messageBodyPart);

				message.setContent(multiPart);
				
				Transport transport = session.getTransport(protocol);
				if (!senderName.isEmpty() && !password.isEmpty())
				{
					transport.connect(senderName, password);
					transport.sendMessage(message, message.getAllRecipients());
					System.out.println("Execution Completed. Email sent Successfully..");
					transport.close();
		        } 
			
			}catch(MessagingException me){
				me.printStackTrace();
			
			}
		}
	}	
	
	/**
	 * Get Current Date
	 * @author MalshaniS
	 *
	 * @return currentDate
	 */
	public String getSystemDate(){
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");	
		Date date = new Date();
		String currentDate = dateFormat.format(date);
		return currentDate;
	}
	
	/**
	 * Get Mail Server Properties
	 * @author MalshaniS
	 *
	 * @param proxyEnabled
	 * @return properties
	 */
	private Properties getMailServerProperties(boolean proxyEnabled){
		Properties properties = System.getProperties();
//		properties.put("mail.smtp.host", "smtp.googlemail.com"); //Gmail
		properties.put("mail.smtp.host", "smtp-mail.outlook.com"); //Outlook
		
		properties.put("mail.smtp.auth", true);
		properties.put("mail.smtp.starttls.enable", true);
		properties.put("mail.smtp.port", "587");
		
		if(proxyEnabled){
			properties.put("http.useProxy","true");
			properties.put("http.proxyPort",proxyPort);
			properties.put("http.proxyHost",proxyHost);
			properties.put("http.proxyUser", proxyUser);
			properties.put("http.proxyPassword", proxyPassword);
			if (sslTrust != null){
				properties.put("mail.smtp.ssl.trust", "*");
			}
	    }
		return properties;
	}
	
	/**
	 * Get Session
	 * @author MalshaniS
	 *
	 * @return session
	 */
    private Session getSession(){
        Properties properties = getMailServerProperties(false);
        
        Session session = Session.getDefaultInstance(properties);

        return session;
    }

    /**
     * Get Session with proxy
     * @author MalshaniS
     *
     * @return session
     */
    private Session getSessionProxy(){
    	
        Properties properties = getMailServerProperties(true);
        
        Session session = Session.getDefaultInstance(properties,
        		new Authenticator(){
        	protected PasswordAuthentication getPasswordAuthentication() {
        	return new PasswordAuthentication(proxyUser, proxyPassword);
        	}});

        return session;
    }

}


