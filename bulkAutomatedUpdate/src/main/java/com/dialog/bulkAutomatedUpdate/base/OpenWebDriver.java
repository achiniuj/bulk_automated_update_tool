package com.dialog.bulkAutomatedUpdate.base;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

	// TODO: Auto-generated Javadoc
	/**
	 * The Class OpenWebDriver.
	 *
	 * @author AchiniUj
	 * The Class OpenWebDriver.
	 */
	public class OpenWebDriver  {
	 
		/** The driver. */
		private WebDriver driver;
		
		
		/**
		 * Open crome browser.
		 *
		 * @return the web driver
		 * @throws Exception the exception
		 */
		public WebDriver openCromeBrowser(String application) throws Exception {
			String url;
			ReadPropertyFileData propFile=new ReadPropertyFileData();
			String driverPath=propFile.getPropertyValue("driverPath");	
			if(application=="Hotline"){
				url=propFile.getPropertyValue("hotlinepageurl");
			}else if(application=="CxRegistrationPage"){//TODO:: Need to remove after get access to GSM Registration
				url=propFile.getPropertyValue("cxRegistrationpageurl");
			}
			else{
				url=propFile.getPropertyValue("crmmainpageurl");
			}	
	        System.setProperty("webdriver.chrome.driver", driverPath);
	        WebDriver driver = new ChromeDriver();  
	        driver.manage().window().maximize();
	        driver.get(url);
	        return driver;
		}
	 
		/**
		 * Save close browser.
		 */
		public void CloseBrowser(){
		    driver.quit();
		}
		  
		
}
