package com.dialog.bulkAutomatedUpdate.pageobjects;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


// TODO: Auto-generated Javadoc
/**
 * The Class CCBSHomePage.
 */
public class CCBSHomePage {
	
	/** The driver. */
	WebDriver driver;

	/**
	 * Instantiates a new CCBS home page.
	 *
	 * @param driver the driver
	 */
	public CCBSHomePage (WebDriver driver){
		this.driver= driver;
	}
	
	/** The txt mob no. */
	By txtMobNo=By.xpath("//label[contains(.,'Mobile No')]/../..//input");
	
	/** The txt account no. */
	By txtAccountNo=By.xpath("//label[contains(.,'Account ID')]/../..//input");
	
	/** The btn mob no search. */
	By btnMobNoSearch=By.xpath("//button[contains(@accesskey,'M')]");
	
	/** The btn search account id. */
	By btnSearchAccountId=By.xpath("//button[contains(@accesskey,'A')]");
	
	/** The btn update. */
	By btnUpdate=By.xpath("//button[@accesskey='U']");
	
	/** The txt cont no. */
	By txtContNo=By.xpath("//label[text()='Cont.No']/../following-sibling::td/input");
	
	/** The lbl modify. */
	By lblModify=By.xpath("//td[text()='Modify']");
	
	/** The btn package change. */
	By btnPackageChange=By.xpath("//button[text()='Package Change']");
	
	/** The txt swith status. */
	By txtSwithStatus=By.xpath("//span[@title[contains(.,'More Connection Switch Status')]]");
	
	/** The tbl Contac no right click menu. */
	By tblContNo=By.xpath("//table[contains(@id,'mPrfNm::ScrollContent')]/tbody");

	/**The txt Accounts - AC Id*/
	By txtAcId = By.xpath("//label[text()='A/C ID']/../input");
	
	/**The lbl Account Updates*/
	By lblAccountUpdates = By.xpath("//td[text()='Account Updates']");
	
	/**The lbl Cx Ref*/
	By lblCxRef = By.xpath("//span[@title[contains(.,'More Mobile No Details')]]");
	
	/**The lbl Ebill Configuration*/
	By lblEbillConfirmation = By.xpath("//table[contains(@id,'m1::ScrollContent')]/tbody//td[text()='E-Bill Confirmation']");
		
	/**
	 * Search mobile no.
	 *
	 * @param mobileNo the mobile no
	 * @throws InterruptedException 
	 */
	public void CCBSSearch(String searchNoType,String searchNo) throws InterruptedException {	
		//Thread.sleep(2000); Test Environment
		
		Thread.sleep(1000); //Live

		if(searchNoType.equalsIgnoreCase("accountNo")){
			driver.findElement(txtAccountNo).clear();
			driver.findElement(txtAccountNo).sendKeys(searchNo);
			driver.findElement(btnSearchAccountId).click();
			driver.manage().timeouts().implicitlyWait(12, TimeUnit.SECONDS);
		}else{
			driver.findElement(txtMobNo).clear();
			driver.findElement(txtMobNo).sendKeys(searchNo);
			driver.findElement(btnMobNoSearch).click();	
			driver.manage().timeouts().implicitlyWait(12, TimeUnit.SECONDS);
		}
		Thread.sleep(5000); //Live
		
		// Thread.sleep(9000);  Test Environment
		
	}
	
	/**
	 * Select from cont no right click.
	 *
	 * @param menue the menue
	 * @throws InterruptedException 
	 */
	public void selectFromContNoRightClick(String menue) throws InterruptedException{
		Actions oAction = new Actions(driver);
		oAction.contextClick(driver.findElement(txtContNo)).build().perform();  	
		
		Thread.sleep(4000);
		WebDriverWait wait = new WebDriverWait(driver, 800000);
		WebElement table_element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table[contains(@id,'mPrfNm::ScrollContent')]/tbody")));
		 ArrayList<WebElement> rows = (ArrayList<WebElement>) table_element.findElements(By.tagName("tr"));
		 for (int i=1;i<=rows.size();i++){
			 WebElement cell=driver.findElement(By.xpath("//table[contains(@id,'mPrfNm::ScrollContent')]/tbody/tr["+i+"]/td[2]"));
			 if(cell.getText().equalsIgnoreCase(menue)){
				 cell.click();
			 	break;		 
			 }
		 }
	}
	
	/**
	 * Click Update Button
	 * @author MalshaniS
	 * @throws InterruptedException 
	 *
	 */
	public void clickUpdate() throws InterruptedException{
		try{
			Thread.sleep(5000);
			WebDriverWait wait = new WebDriverWait(driver, 500000);
			WebElement eleUpdate = wait.until(ExpectedConditions.visibilityOf(driver.findElement(btnUpdate)));
			eleUpdate.click();

		}catch(StaleElementReferenceException  e){
			e.getMessage();
		}
	}
		
	/**
	 * Click Account Updates
	 * @author MalshaniS
	 *
	 */
	public void clickAccountUpdates(){
		driver.findElement(lblAccountUpdates).click();
	}
		
	/**
	 * Click E-bill Confirmation
	 * @author MalshaniS
	 *
	 */
	public void clickEbillConfirmation(){
		driver.findElement(lblEbillConfirmation).click();
	}
	
	/**
	 * Select from AC Id right click.
	 *
	 * @param menue the menue
	 */
	public void selectFromAcIdRightClick(String menue){
		Actions oAction = new Actions(driver);
		oAction.contextClick(driver.findElement(txtAcId)).build().perform();  
		driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
		
		WebDriverWait wait = new WebDriverWait(driver, 600000);
		WebElement table_element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table[contains(@id,'mAccId::ScrollContent')]/tbody")));
		ArrayList<WebElement> rows = (ArrayList<WebElement>) table_element.findElements(By.tagName("tr"));
		 for (int i=1;i<=rows.size();i++){
			 WebElement cell=driver.findElement(By.xpath("//table[contains(@id,'mAccId::ScrollContent')]/tbody/tr["+i+"]/td[2]"));
			 if(cell.getText().equalsIgnoreCase(menue)){
				 cell.click();
				 break;	
			 } 	 
		 }
	}

	/**
	 * Select from Cx Reg right click.
	 *
	 * @param menue the menue
	 * @throws InterruptedException 
	 */
	public void selectFromCxRegRightClick(String menue) throws InterruptedException{
		Actions oAction = new Actions(driver);
		oAction.contextClick(driver.findElement(lblCxRef)).build().perform();  	
		
		Thread.sleep(5000);
		WebDriverWait wait = new WebDriverWait(driver, 600000);
		WebElement table_element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class,'AFPopupMenuPopup')]//table[contains(@id,'m1::ScrollContent')]/tbody")));
		ArrayList<WebElement> rows = (ArrayList<WebElement>) table_element.findElements(By.tagName("tr"));
		 for (int i=1;i<=rows.size();i++){
			 WebElement cell=driver.findElement(By.xpath("//div[contains(@class,'AFPopupMenuPopup')]//table[contains(@id,'m1::ScrollContent')]/tbody/tr["+i+"]/td[2]"));
			 if(cell.getText().equalsIgnoreCase(menue)){
				 cell.click();
				 break;	
			 } 	 
		 }
	}
	
	/**
	 * Click Package Change
	 * @author MalshaniS
	 *
	 */
	public void clickPackageChange(){
		WebDriverWait wait = new WebDriverWait(driver, 600000);
		WebElement elePackageChange = wait.until(ExpectedConditions.visibilityOf(driver.findElement(btnPackageChange)));
		elePackageChange.click();
	}
}
