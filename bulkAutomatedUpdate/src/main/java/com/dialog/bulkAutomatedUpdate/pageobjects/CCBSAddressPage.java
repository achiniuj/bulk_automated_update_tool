package com.dialog.bulkAutomatedUpdate.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

// TODO: Auto-generated Javadoc
/**
 * The Class CCBSAddressPage.
 */
public class CCBSAddressPage {
	
	/**The driver*/
	WebDriver driver;
	
	/**
	 * Instantiates a new CCBS Address Page.
	 * 
	 * @param driver
	 */
	public CCBSAddressPage(WebDriver driver){
		this.driver = driver;
	}

	/** The lbl cx name. */
	By lblCxName=By.xpath("//label[contains(.,'Customer Name')]/../..//strong");
	
	/** The lbl id type. */
	By lblIdNo=By.xpath("//label[contains(.,'Identification')]/../..//strong");
	
	/** The txt name to appera on bill. */
	By txtNameToApperaOnBill=By.xpath("//label[contains(.,'Name to Appear on Bill')]/../..//input");
	
	/** The txt address line1. */
	By txtAddressLine1=By.xpath("//label[contains(.,'Line1')]/../input");
	
	/** The txt address line2. */
	By txtAddressLine2=By.xpath("//label[contains(.,'Line 2')]/../..//input");
	
	/** The txt address line3. */
	By txtAddressLine3=By.xpath("//label[contains(.,'Line 3')]/../..//input");
	
	/** The dd postal code. */
	By ddPostalCode=By.xpath("//label[contains(.,'Postal Code')]/../..//span/input");
	
	/** The dd address type. */
	By ddAddressType=By.xpath("//label[contains(.,'AddressType')]/../..//select");
	
	/** The txt cont no. */
	By txtContNo=By.xpath("//label[contains(.,'Contact No')]/../..//input");
	
	/** The txt email address. */
	String emailAddress= "//nobr[text()='%s']/../..//label[contains(.,'EmailAddress')]/../..//input";
	
	/** The btn save. */
	By btnSave=By.xpath("//button[text()='Save']");
	
	/**The lbl Information Popup Message*/
	By lblInformationPopupMsg = By.xpath("//div[contains(@id,'msgDlg::_cnt')]/div/table/tbody//div");
	
	/**The btn Popup OK*/
	By btnPopupOk = By.xpath("//button[contains(@id,'msgDlg::cancel')]");
	
	/**The btn Create New*/
	By btnCreateNew = By.xpath("//button[text()='Create New']");
	
	/**The txt Create Address Line 1*/
	By txtCreateAddressLine1 = By.xpath("//label[text()='Line 1']/../..//input");
	
	/**The txt Create Address Line 2*/
	By txtCreateAddressLine2 = By.xpath("//label[text()='Line 2']/../..//input");
	
	/**The txt Create Address Line 3*/
	By txtCreateAddressLine3 = By.xpath("//label[text()='Line 3']/../..//input");
	
	/**The btn Search Postal Code*/
	By btnSearchPostalCode = By.xpath("//a[@title='Search: Postal Code']");
	
	/**The lnk Search*/
	By lnkSearch = By.xpath("//a[contains(.,'Search')]");
	
	/**The txt Create Address Postal Code*/
	By txtCreateAddressPostalCode = By.xpath("//label[text()='PostalCode']/../..//input");
	
	/**The btn Search*/
	By btnSearch = By.xpath("//button[text()='Search']");
	
	/**The ele Postal Code*/
//	String elePostalCode = "//div[contains(@id,'postalCodeId')]//table/tbody/tr/td/nobr[text()='%s']"; //Test Environment
	String elePostalCode = "//div[contains(@id,'postalCodeId_afrLovInternalTableId')]//table/tbody/tr/td[2]/nobr[text()='%s']"; //Live Environment
	
	
	/**The btn OK Postal Code List*/
	By btnOkPostalCodeList = By.xpath("//div[contains(@id,'postalCodeId')]//button[text()='OK']");
	
	/**The btn Back To Hotline*/
	By btnBackToHotlineCreateAddressPage = By.xpath("//button[text()='Back To Hotline']");
	
	/**The btn BAck To Hotline Address Change Page*/
	By btnBackToHotlineAddressChangePage = By.xpath("//button[@accesskey='B']"); 
	
	/**The btn Save Address Change*/
	By btnSaveAddressChange = By.xpath("//button[@accesskey='S']");
	
	/**
	 * Fill Name To Appera On Bill
	 * @author MalshaniS
	 *
	 * @param cxName
	 */
	public void fillNameToApperaOnBill(String cxName){
		driver.findElement(txtNameToApperaOnBill).clear();
		driver.findElement(txtNameToApperaOnBill).sendKeys(cxName);
	}
	
	/**
	 * Click Save
	 * @author MalshaniS
	 * @throws InterruptedException 
	 *
	 */
	public void clickSave() throws InterruptedException{
		Thread.sleep(4000);
		WebDriverWait wait = new WebDriverWait(driver, 600000);
		WebElement eleSave = wait.until(ExpectedConditions.visibilityOf(driver.findElement(btnSave)));
		eleSave.click();
	}
	
	/**
	 * Get Information Popup Message
	 * @author MalshaniS
	 *
	 * @return popupMessage
	 * @throws InterruptedException 
	 */
	public String getPopupMessage() throws InterruptedException{
		Thread.sleep(4000);
		WebDriverWait wait = new WebDriverWait(driver, 600000);
		WebElement eleInformationPopup = wait.until(ExpectedConditions.visibilityOf(driver.findElement(lblInformationPopupMsg)));
		String message = eleInformationPopup.getText();
		clickPopupOk();
		return message;
	}
	
	/**
	 * Click Popup Ok
	 * @author MalshaniS
	 *
	 */
	public void clickPopupOk(){
		driver.findElement(btnPopupOk).click();
	}
	
	/**
	 * Fill Email Address
	 * @author MalshaniS
	 *
	 * @param email
	 */
	public void fillEmailAddress(String addressId, String email){
		By txtEmailAddress = By.xpath(String.format(emailAddress, addressId));
		driver.findElement(txtEmailAddress).clear();
		driver.findElement(txtEmailAddress).sendKeys(email);
	}
	
	/**
	 * Get Existing Name To Appear On Bill
	 * @author MalshaniS
	 *
	 * @return NameToApperaOnBill
	 */
	public String getNameToApperaOnBill(){
		return driver.findElement(txtNameToApperaOnBill).getAttribute("value").trim();
	}
	
	/**
	 * Get Email
	 * @author MalshaniS
	 *
	 * @param addressId
	 * @return email
	 */
	public String getEmailAddress(String addressId){
		By txtEmailAddress = By.xpath(String.format(emailAddress, addressId));
		return driver.findElement(txtEmailAddress).getAttribute("value");
	}

	/**
	 * Get Address Line 1
	 * @author MalshaniS
	 *
	 * @return addressLine1
	 */
	public String getAddressLine1(){
		return driver.findElement(txtAddressLine1).getAttribute("value");
	}
	
	/**
	 * Get Address Line 2
	 * @author MalshaniS
	 *
	 * @return addressLine2
	 */
	public String getAddressLine2(){
		return driver.findElement(txtAddressLine2).getAttribute("value");
	}
	
	/**
	 * Get Address Line 3
	 * @author MalshaniS
	 *
	 * @return addressLine3
	 */
	public String getAddressLine3(){
		return driver.findElement(txtAddressLine3).getAttribute("value");
	}
	
	/**
	 * Get Postal Code
	 * @author MalshaniS
	 *
	 * @return postalCode
	 */
	public String getPostalCode(){
		return driver.findElement(ddPostalCode).getAttribute("value");
	}

	/**
	 * Click Create New
	 * @author MalshaniS
	 *
	 */
	public void clickCreateNew(){
		driver.findElement(btnCreateNew).click();
	}
	
	/**
	 * Fill New Address Line 1
	 * @author MalshaniS
	 *
	 * @param addressLine1
	 */
	public void fillNewAddressLine1(String addressLine1){
		driver.findElement(txtCreateAddressLine1).sendKeys(addressLine1);
	}
	
	/**
	 * Fill New Address Line 2
	 * @author MalshaniS
	 *
	 * @param addressLine2
	 */
	public void fillNewAddressLine2(String addressLine2){
		driver.findElement(txtCreateAddressLine2).sendKeys(addressLine2);
	}
	
	/**
	 * Fill New Address Line 3
	 * @author MalshaniS
	 *
	 * @param addressLine3
	 */
	public void fillNewAddressLine3(String addressLine3){
		driver.findElement(txtCreateAddressLine3).sendKeys(addressLine3);
	}
	
	/**
	 * Select Postal Code
	 * @author MalshaniS
	 *
	 * @param postalCode
	 * @throws InterruptedException 
	 */
	public void selectNewPostalCode(String postalCode) throws InterruptedException{
		driver.findElement(btnSearchPostalCode).click();

		WebDriverWait wait = new WebDriverWait(driver, 600000);
		WebElement eleSearch = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(lnkSearch)));
		eleSearch.click();
		Thread.sleep(1000);
		
		driver.findElement(txtCreateAddressPostalCode).sendKeys(postalCode);
		driver.findElement(btnSearch).click();
		Thread.sleep(1000);
		
		By lblPostalCode = By.xpath(String.format(elePostalCode, postalCode));
		driver.findElement(lblPostalCode).click();
		Thread.sleep(1000);
		
		driver.findElement(btnOkPostalCodeList).click();
		Thread.sleep(1000);
	}

	/**
	 * Select Address Type
	 * @author MalshaniS
	 *
	 * @param addressType
	 */
	public void selectAddressType(String addressType){
		Select objSelect = new Select(driver.findElement(ddAddressType));
		objSelect.selectByVisibleText(addressType);
	}

	/**
	 * String get Information Popup Msg
	 * @author MalshaniS
	 *
	 * @return
	 */
	public String getInformationPopupMsg(){
		WebDriverWait wait = new WebDriverWait(driver, 600000);
		WebElement eleInformationPopup = wait.until(ExpectedConditions.visibilityOf(driver.findElement(lblInformationPopupMsg)));
		String popupMessage = eleInformationPopup.getText();
		clickPopupOk();
		return popupMessage;
	}
	
	/**
	 * Get New Address Id 
	 * @author MalshaniS
	 *
	 * @param message
	 * @return addressId
	 */
	public String getNewAddressId(String message){
		String[] arrMessage = message.split("=");
		String addressId = arrMessage[1].substring(0, 9).trim();
		return addressId;
	}
	
	/**
	 * Click Back To Hotline from Create Address Page
	 * @author MalshaniS
	 *
	 */
	public void clickBackToHotlineCreateAddressPage(){
		driver.findElement(btnBackToHotlineCreateAddressPage).click();
	}
	
	/**
	 * Click Back To Hotline Address Change Page
	 * @author MalshaniS
	 *
	 */
	public void clickBackToHotlineAddressChangePage(){
		driver.findElement(btnBackToHotlineAddressChangePage).click();
	}

	/**
	 * Click Save Address Change
	 * @author MalshaniS
	 * @throws InterruptedException 
	 *
	 */
	public void clickSaveAdressChange() throws InterruptedException{
		Thread.sleep(3000);
		WebDriverWait wait = new WebDriverWait(driver, 600000);
		WebElement eleSaveAddressChange = wait.until(ExpectedConditions.visibilityOf(driver.findElement(btnSaveAddressChange)));
		eleSaveAddressChange.click();
		Thread.sleep(2000);
	}
}
