package com.dialog.bulkAutomatedUpdate.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


// TODO: Auto-generated Javadoc
/**
 * The Class DialogCRMMainHomePage.
 * @author Malshani Senarathne.
 */
public class CRMMainHomePage {

	/** The driver. */
	WebDriver driver;
	
	/**
	 * Instantiates a new CRMMainHomePage page.
	 *
	 * @param driver the driver
	 */
	public CRMMainHomePage(WebDriver driver){
		this.driver = driver;
	}

	
	/** The Home Page Header. */
	By lblHomePageHeader = By.xpath("//div/table/tbody/tr/td[@valign='top']/span[1]");
	
	/** The lbl User. */
	By lblUserName = By.xpath("//div/table/tbody/tr[2]/td/span[2]");
	
	

	/**
	 * Crm main menu navigation.
	 *
	 * @param mainMenu the main menu
	 * @throws InterruptedException 
	 */
	public void crmMainMenuNavigation(String mainMenu) throws InterruptedException{
		
		/**The dd Main Menu*/
		Thread.sleep(4000);
		WebDriverWait wait = new WebDriverWait(driver, 500000);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(lblHomePageHeader)));
		WebElement ddmaiMenu  = driver.findElement(By.xpath("//a[contains(text(),'"+mainMenu+"')]/../../td[3]"));
		ddmaiMenu.click();

	}
		
	/**	
	 * Crm sub menu navigation.
	 *
	 * @author AchiniUJ
	 * @param subMenu the sub menu
	 * @throws InterruptedException 
	 */
	public void crmSubMenuNavigation(String subMenu) throws InterruptedException{
		/**The dd Main Menu*/
		Thread.sleep(3000);
		WebDriverWait wait = new WebDriverWait(driver, 500000);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(lblHomePageHeader)));
		WebElement ddSubMenu  = driver.findElement(By.xpath("//td[text()='"+subMenu+"']"));
		ddSubMenu.click();
		Thread.sleep(1000);
	}
}
