package com.dialog.bulkAutomatedUpdate.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class CCBSAccountUpdatePage {
	
	/**The driver*/
	WebDriver driver;
	
	/**
	 * Instantiates a new CCBS Account Update Page.
	 * 
	 * @param driver
	 */
	public CCBSAccountUpdatePage(WebDriver driver){
		this.driver = driver;
	}
	
	/**The txt Description*/
	String eleDescription = "//span[text()='%s']/../../../..//label[text()='Description']/../input";
	
	/**The txt Address Id*/
	String eleAddressId = "//span[text()='%s']/../../../..//input[contains(@id,'addressId')]";
	
	/**The dd Bill Dispatch Type*/
	String eleBillDispatchType = "//span[text()='%s']/../../../..//label[text()='BillDespatchTypeId']/../select";
	
	/**The btn Save*/
	By btnSave = By.xpath("//button[@accesskey='S']");
	
	/**The lbl Information Popup Message*/
	By lblInformationPopupMsg = By.xpath("//div[contains(@id,'msgDlg::_cnt')]/div/table/tbody//div");
	
	/**The btn Popup OK*/
	By btnPopupOk = By.xpath("//button[contains(@id,'msgDlg::cancel')]");
	
	/**The btn Back To Hotline*/
	By btnBackToHotline = By.xpath("//button[@accesskey='B']"); 
	
	/**
	 * Fill Description
	 * @author MalshaniS
	 *
	 * @param nodeId
	 * @param cxName
	 */
	public void fillDescription(String nodeId, String cxName){
		By txtDescription = By.xpath(String.format(eleDescription, nodeId));
		driver.findElement(txtDescription).clear();
		driver.findElement(txtDescription).sendKeys(cxName);
	}
	
	/**
	 * Fill Address Id
	 * @author MalshaniS
	 *
	 * @param nodeId
	 * @param addressId
	 */
	public void fillAddressId(String nodeId, String addressId){
		By txtAddressId = By.xpath(String.format(eleAddressId, nodeId));
		driver.findElement(txtAddressId).clear();
		driver.findElement(txtAddressId).sendKeys(addressId);
	}
	
	/**
	 * Select Bill Dispatch Type
	 * @author MalshaniS
	 *
	 * @param nodeId
	 * @param billDispatchType
	 */
	public void selectBillDispatchType(String nodeId, String billDispatchType){
		By ddBillDispatchType = By.xpath(String.format(eleBillDispatchType, nodeId));
		Select objSelect = new Select(driver.findElement(ddBillDispatchType));
		objSelect.selectByVisibleText(billDispatchType);
	}

	/**
	 * Click Save
	 * @author MalshaniS
	 *
	 */
	public void clickSave(){
		driver.findElement(btnSave).click();
	}
	
	/**
	 * Get Information Popup Message
	 * @author MalshaniS
	 *
	 * @return popupMessage
	 * @throws InterruptedException 
	 */
	public String getPopupMessage() throws InterruptedException{
		Thread.sleep(3000);
		WebDriverWait wait = new WebDriverWait(driver, 600000);
		WebElement eleInformationPopup = wait.until(ExpectedConditions.visibilityOf(driver.findElement(lblInformationPopupMsg)));
		String message = eleInformationPopup.getText();
		clickPopupOk();
		return message;
	}
	
	/**
	 * Click Popup Ok
	 * @author MalshaniS
	 *
	 */
	public void clickPopupOk(){
		WebDriverWait wait = new WebDriverWait(driver, 600000);
		WebElement elePopupOk = wait.until(ExpectedConditions.visibilityOf(driver.findElement(btnPopupOk)));
		elePopupOk.click();
	}
	
	/**
	 * Get Existing Description
	 * @author MalshaniS
	 *
	 * @param nodeId
	 * @return description
	 */
	public String getDescription(String nodeId){
		By txtDescription = By.xpath(String.format(eleDescription, nodeId));
		return driver.findElement(txtDescription).getAttribute("value");
	}
	
	/**
	 * Get Bill Dispatch Type 
	 * @author MalshaniS
	 *
	 * @param nodeId
	 * @return
	 */
	public String getBillDispatchType(String nodeId){
		By ddBillDispatchType = By.xpath(String.format(eleBillDispatchType, nodeId));
		return driver.findElement(ddBillDispatchType).getAttribute("title");
	}

	/**
	 * Click Back to Hotline
	 * @author MalshaniS
	 *
	 */
	public void clickBackToHotline(){
		WebDriverWait wait = new WebDriverWait(driver, 600000);
		WebElement eleBackToHotline = wait.until(ExpectedConditions.visibilityOf(driver.findElement(btnBackToHotline)));		
		eleBackToHotline.click();
	}
}
