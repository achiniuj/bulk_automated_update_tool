package com.dialog.bulkAutomatedUpdate.pageobjects;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

// TODO: Auto-generated Javadoc
/**
 * The Class CCBSSubscriberProfilesUpdatePage.
 */
public class CCBSSubscriberProfilesUpdatePage {
	/** The driver. */
	WebDriver driver;

	/**
	 * Instantiates a new CCBS subscriber profiles update page.
	 *
	 * @param driver the driver
	 */
	public CCBSSubscriberProfilesUpdatePage (WebDriver driver){
		this.driver= driver;
	}
	
	/** The dd id type. */
	By ddIdType=By.xpath("//select[contains(@id,'socIdentificationType::content')]");
	
	/** The btn update. */
	By btnUpdate=By.xpath("//button[@accesskey='U']");
	
	/** The txt pop msg. */
	By txtPopMsg=By.xpath("//div[text()='Information']/../../../../../../../tr[2]//table/tbody/tr/td[2]/div");
	
	/** The txt pop up header. */
	By txtPopUpHeader=By.xpath("//a[@title='Close']/../../../td[2]/div");
	
	/** The txt id no. */
	By txtIdNo=By.xpath("//label[text()='Identification Number']/../../td[2]//td[2]/input");

	/** The dd language. */
	By ddLanguage=By.xpath("//select[@name[contains(.,'CommunicationLanguage')]]");
	
	/** The txt contact name. */
	By txtContactName=By.xpath("//label[text() ='Contact Name']/../..//td[2]//input");
	
	/** The txt contact no. */
	By txtContactNo=By.xpath("//label[text() ='Contact Phone Number']/../..//td[2]//input");
	
	/** The dd segment code. */
	By ddSegmentCode=By.xpath("//label[text() ='Segmentation Code']/../..//td[2]/select");

	/** The dd Title. */
	By ddTitle = By.xpath("//label[text()='Title']/../..//select");
	
	/** The txt Surname. */
	By txtSurname = By.xpath("//label[text()='Surname ']/../..//input");
	
	/** The txt Other Names. */
	By txtOtherName = By.xpath("//label[text()='Other Names']/../..//input");
	
	/** The txt reg date. */
	By txtRegDate=By.xpath("//input[contains(@id,'idDobRegistrationDate::content')]");

	By btnBackToHotline = By.xpath("//button[@accesskey='B']");
	
	By btnPopupOk = By.xpath("//button[contains(@id,'msgDlg::cancel')]");
	
	/**
	 * Select id type.
	 *
	 * @param idType the id type
	 */
	public void selectIdType(String idType){
		Select dropDown=new Select(driver.findElement(ddIdType));
		dropDown.selectByVisibleText(idType);
	}
		
	/**
	 * Click update.
	 */
	public void clickUpdate(){
		driver.findElement(btnUpdate).click();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	/**
	 * Gets the pop up msg.
	 *
	 * @return the pop up msg
	 */
	public String getPopUpMsg(){
		String message=null;
		
		WebDriverWait wait = new WebDriverWait(driver, 1000);
		WebElement lblPopupHeader = wait.until(ExpectedConditions.visibilityOf(driver.findElement(txtPopUpHeader)));
		
		if(lblPopupHeader.getText().equalsIgnoreCase("Error")){		
			String errorMessage1=driver.findElement(By.xpath("//div[text()='Messages for this page are listed below.']/../table/tbody//tbody/tr/td[2]/div")).getText();
			String errorMessage2=driver.findElement(By.xpath("//div[text()='Messages for this page are listed below.']/../table/tbody//tbody/tr[2]/td[2]/div")).getText();
			message=errorMessage1+" "+errorMessage2;
		}else{
			message=driver.findElement(txtPopMsg).getText().trim();
		}
		driver.findElement(btnPopupOk).click();
		return message;
	}
	
	/**
	 * Change id no.
	 *
	 * @param idNo the id no
	 */
	public void changeIdNo(String idNo){
		driver.findElement(txtIdNo).clear();
		driver.findElement(txtIdNo).sendKeys(idNo);
	}
	
	/**
	 * Gets the id no.
	 *
	 * @return the id no
	 */
	public String getIdNO(){
		return driver.findElement(txtIdNo).getAttribute("title").trim();
	}
	
	/**
	 * Select language.
	 *
	 * @param language the language
	 */
	public void selectLanguage(String language){
		Select dropDown=new Select(driver.findElement(ddLanguage));
		dropDown.selectByVisibleText(language);	
	}
	
	/**
	 * Change contact person.
	 *
	 * @param contactName the contact name
	 * @param contactNo the contact no
	 */
	public void changeContactPerson(String contactName){
		driver.findElement(txtContactName).clear();
		driver.findElement(txtContactName).sendKeys(contactName);
	}
	
	/**
	 * Change Contact No
	 * @author MalshaniS
	 *
	 * @param contactNo
	 */
	public void changeContactNo(String contactNo){
		driver.findElement(txtContactNo).clear();
		driver.findElement(txtContactNo).sendKeys(contactNo);
	}
	
	/**
	 * Change segment code.
	 *
	 * @param segmentCode the segment code
	 */
	public void changeSegmentCode(String segmentCode){
		Select dropDown=new Select(driver.findElement(ddSegmentCode));
		dropDown.selectByVisibleText(segmentCode);
	}
	
	/**
	 * Select Title.
	 *
	 * @author MalshaniS
	 * @param title the title
	 */
	public void selectTitle(String title){
		Select objSelect = new Select(driver.findElement(ddTitle));
		objSelect.selectByVisibleText(title);
	}
	
	/**
	 * Fill Surname.
	 *
	 * @author MalshaniS
	 * @param surname the surname
	 */
	public void fillSurname(String surname){
		driver.findElement(txtSurname).clear();
		driver.findElement(txtSurname).sendKeys(surname);
	}
	
	/**
	 * Fill Other Name.
	 *
	 * @author MalshaniS
	 * @param otherName the other name
	 */
	public void fillOtherName(String otherName){
		driver.findElement(txtOtherName).clear();
		driver.findElement(txtOtherName).sendKeys(otherName);

	}
	
	/**
	 * Change dob or dor.
	 *
	 * @param Date the date
	 */
	public void changeDOBOrDOR(String Date){
		driver.findElement(txtRegDate).clear();
		driver.findElement(txtRegDate).sendKeys(Date);
	}
	
	/**
	 * Get Selected Title
	 * @author MalshaniS
	 *
	 * @return title
	 */
	public String getTitle(){
		return driver.findElement(ddTitle).getAttribute("title").trim();
	}
	
	/**
	 * Get Surname
	 * @author MalshaniS
	 *
	 * @return surname
	 */
	public String getSurname(){
		return driver.findElement(txtSurname).getAttribute("value").trim();
	}
	
	/**
	 * Get Othername
	 * @author MalshaniS
	 *
	 * @return othername
	 */
	public String getOtherName(){
		return driver.findElement(txtOtherName).getAttribute("value").trim();

	}
	
	/**
	 * Get Selected Id Type
	 * @author MalshaniS
	 *
	 * @return idType
	 */
	public String getIdType(){
		return driver.findElement(ddIdType).getAttribute("title");
	}

	/**
	 * Get Existing ID Number
	 * @author MalshaniS
	 *
	 * @return Id Number
	 */
	public String getIdNo(){
		return driver.findElement(txtIdNo).getAttribute("value");
	}

	/**
	 * Get Language
	 * @author MalshaniS
	 *
	 * @return language
	 */
	public String getLanguage(){
		return driver.findElement(ddLanguage).getAttribute("title");
	}

	/**
	 * Get Contact Person
	 * @author MalshaniS
	 *
	 * @return contact person
	 */
	public String getContactPerson(){
		return driver.findElement(txtContactName).getAttribute("value");
	}
	
	/**
	 * Get Contact Person Number
	 * @author MalshaniS
	 * @return contact No
	 *
	 */
	public String getContactPersonNumber(){
		return driver.findElement(txtContactNo).getAttribute("value");
	}
	
	/**
	 * Get Segment Code
	 * @author MalshaniS
	 *
	 * @return SegmentCode
	 */
	public String getSegmentCode(){
		return driver.findElement(ddSegmentCode).getAttribute("title");
	}

	/**
	 * Get DOB or DOR
	 * @author MalshaniS
	 *
	 * @return dob or dor
	 */
	public String getDOBOrDOR(){
		return driver.findElement(txtRegDate).getAttribute("value");
	}

	/**
	 * Click Back to hotline
	 * @author MalshaniS
	 *
	 */
	public void clickBackToHotline(){
		driver.findElement(btnBackToHotline).click();
	}
}
