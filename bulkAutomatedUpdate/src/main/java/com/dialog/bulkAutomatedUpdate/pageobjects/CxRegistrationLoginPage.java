package com.dialog.bulkAutomatedUpdate.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

// TODO: Auto-generated Javadoc
/**
 * The Class CxRegistrationLoginPage.
 */
public class CxRegistrationLoginPage {

	/** The driver. */
	WebDriver driver;
	
	/**
	 * Instantiates a new cx registration login page.
	 *
	 * @param driver the driver
	 */
	public CxRegistrationLoginPage(WebDriver driver){
		this.driver = driver;
	}
	
	/** The txt user name. */
	By txtUserName=By.xpath("//input[@name='username']");
	
	/** The txt password. */
	By txtPassword=By.xpath("//input[@name='password']");
	
	/** The btn login. */
	By btnLogin=By.xpath("//input[@value='Login']");
	
	public void loginCxRegistrationPage(String userName, String password) throws InterruptedException{
		driver.findElement(txtUserName).sendKeys(userName);
		driver.findElement(txtPassword).sendKeys(password);
		driver.findElement(btnLogin).click();
		Thread.sleep(3000);
	}
	
}
