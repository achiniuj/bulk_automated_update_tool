package com.dialog.bulkAutomatedUpdate.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CCBSEbillConfirmationPage {
	
	/**The driver*/
	WebDriver driver;
	
	/**
	 * Instantiates a new CCBS Ebill Confirmation Page.
	 * 
	 * @param driver
	 */
	public CCBSEbillConfirmationPage(WebDriver driver){
		this.driver = driver;
	}

	/**The txt Email Address*/
	By txtEmailAddress = By.xpath("//label[text()='Email Address']/../..//input");
	
	/**The dd Bill Type*/
	By ddBillType = By.xpath("//label[text()='Bill Type']/../..//select");
	
	/**The dd Bill Request*/
	By ddBillRequest = By.xpath("//label[text()='Bill Request']/../..//select");
	
	/**The btn Add*/
	By btnAdd = By.xpath("//button[text()='Add']");
	
	/**The lbl Information Popup Message*/
	By lblInformationPopupMsg = By.xpath("//div[contains(@id,'msgDlg::_cnt')]/div/table/tbody//div");
	
	/**The ele Old Bill Request*/
	String eleOldEmailBillRequest = "//input[@value='%s']/../../../..//label[text()='BillRequest']/../select";
	
	/**The btn Save All*/
	By btnSaveAll = By.xpath("//button[@accesskey='S']");
	
	/**The btn Popup OK*/
	By btnPopupOk = By.xpath("//button[contains(@id,'msgDlg::cancel')]");
	
	/**The txt Existing Ebill Email*/
	By txtExistingEbillEmail = By.xpath("//label[text()='BillRequest']/../select[@title='YES']/../../../..//label[text()='EmailAddress']/../input");
		
	/**The btn Back To Hotline*/
	By btnBackToHotline = By.xpath("//button[@accesskey='B']"); 
	
	/**
	 * Fill Email Address
	 * @author MalshaniS
	 *
	 * @param emailAddress
	 * @throws InterruptedException 
	 */
	public void fillEmailAddress(String emailAddress) throws InterruptedException{
		Thread.sleep(5000);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		WebElement eleEmailAddress = wait.until(ExpectedConditions.visibilityOf(driver.findElement(txtEmailAddress)));
		eleEmailAddress.click();
		eleEmailAddress.sendKeys(emailAddress);
	}
	
	/**
	 * Select Bill Type
	 * @author MalshaniS
	 *
	 * @param billType
	 * @throws InterruptedException 
	 */
	public void selectBillType(String billType) throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(driver, 30);
		WebElement eleBillType = wait.until(ExpectedConditions.visibilityOf(driver.findElement(ddBillType)));
		
		Select objSelect = new Select(eleBillType);
		objSelect.selectByVisibleText(billType);
	}
	
	/**
	 * Select Bill Request
	 * @author MalshaniS
	 *
	 * @param billRequest
	 */
	public void selectBillRequest(String billRequest){
		Select objSelect = new Select(driver.findElement(ddBillRequest));
		objSelect.selectByVisibleText(billRequest);
	}
	
	/**
	 * Click Add
	 * @author MalshaniS
	 *
	 */
	public void clickAdd(){
		driver.findElement(btnAdd).click();
	}
	
	/**
	 * Get Information Popup Message
	 * @author MalshaniS
	 *
	 * @return popup Message
	 * @throws InterruptedException 
	 */
	public String getPopupMessage() throws InterruptedException{
		Thread.sleep(2000);
		WebDriverWait wait = new WebDriverWait(driver, 60);
		WebElement eleInformationPopup = wait.until(ExpectedConditions.visibilityOf(driver.findElement(lblInformationPopupMsg)));
		String popupMessage = eleInformationPopup.getText();
		clickPopupOk();
		return popupMessage;
	}
	
	/**
	 * Select Old Bill Request
	 * @author MalshaniS
	 *
	 * @param oldEmail
	 * @param billRequest
	 */
	public void selectOldBillRequest(String oldEmail, String billRequest){
		By ddOldBillRequest = By.xpath(String.format(eleOldEmailBillRequest, oldEmail));
		Select objSelect = new Select(driver.findElement(ddOldBillRequest));
		objSelect.selectByVisibleText(billRequest);
	}
	
	/**
	 * Click Save All
	 * @author MalshaniS
	 * @throws InterruptedException 
	 *
	 */
	public void clickSaveAll() throws InterruptedException{
		Thread.sleep(5000);
		driver.findElement(btnSaveAll).click();
	}
	
	/**
	 * Click Popup Ok
	 * @author MalshaniS
	 *
	 */
	public void clickPopupOk(){
		WebDriverWait wait = new WebDriverWait(driver, 60);
		WebElement elePopupOk = wait.until(ExpectedConditions.visibilityOf(driver.findElement(btnPopupOk)));
		elePopupOk.click();
	}
	
	/**
	 * Get Existing Email
	 * @author MalshaniS
	 *
	 * @return existing email
	 */
	public String getExistingEmail(){
		return driver.findElement(txtExistingEbillEmail).getAttribute("value");
	}
	
	/**
	 * Click Back to Hotline
	 * @author MalshaniS
	 *
	 */
	public void clickBackToHotline(){
		driver.findElement(btnBackToHotline).click();
	}
}
