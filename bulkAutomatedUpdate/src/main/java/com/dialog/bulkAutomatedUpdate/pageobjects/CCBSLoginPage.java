package com.dialog.bulkAutomatedUpdate.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

// TODO: Auto-generated Javadoc
/**
 * The Class CCBSLoginPage.
 */
public class CCBSLoginPage {
	
	/** The driver. */
	WebDriver driver;
	
	
	/**
	 * Instantiates a new CCBS login page.
	 *
	 * @param driver the driver
	 */
	public CCBSLoginPage(WebDriver driver){
		this.driver= driver;
	}
	
	/** The txt userName. */
	By txtUserName=By.xpath("//input[@id='username']");
	
	/** The txt passward. */
	By txtPassward=By.xpath("//input[@id='password']");
	
	/** The btn login. */
	By btnLogin=By.xpath("//input[@value='Login']");
		
	/**
	 * CCBS login.
	 *@author AchiniUj
	 * @param userName the user name
	 * @param passward the passward
	 */
	public void CCBSLogin(String userName, String passward){	
		driver.findElement(txtUserName).sendKeys(userName);
		driver.findElement(txtPassward).sendKeys(passward);
		driver.findElement(btnLogin).click();
	}			
	
    
}
