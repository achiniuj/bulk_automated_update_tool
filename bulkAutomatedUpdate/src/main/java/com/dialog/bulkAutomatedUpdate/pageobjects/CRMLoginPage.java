package com.dialog.bulkAutomatedUpdate.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

// TODO: Auto-generated Javadoc
/**
 * The Class CRMLoginPage.
 */
public class CRMLoginPage {

	/** The driver. */
	WebDriver driver;
	
	/**
	 * Instantiates a new CRMMainHomePage page.
	 *
	 * @param driver the driver
	 */
	public CRMLoginPage(WebDriver driver){
		this.driver = driver;
	}

	/** The txt login. */
	By txtLogin = By.xpath( "//input[@id='username']");	
	
	/** The txt passward. */
	By txtPassword = By.xpath("//input[@id='password']");
	
	/** The btn Login. */
	By btnLogin = By.xpath("//input[@value='Login']");
	
	/**
	 * CCBS login.
	 *
	 * @param userName the user name
	 * @param passward the passward
	 */
	public void CRMMainLogin(String userName, String passward){	
		driver.findElement(txtLogin).sendKeys(userName);
		driver.findElement(txtPassword).sendKeys(passward);
		driver.findElement(btnLogin).click();
	}			
	
}
