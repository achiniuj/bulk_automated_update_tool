package com.dialog.bulkAutomatedUpdate.pageobjects;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CCBSPackageChangePage {

	/**The driver*/
	WebDriver driver;
	
	/**
	 * Instantiates a new Package Change Page.
	 * 
	 * @param driver
	 */
	public CCBSPackageChangePage(WebDriver driver){
		this.driver = driver;
	}
	
	/**The lbl Package Code*/
	By lblPackageCode = By.xpath("//label[text()='Package Code']/../../following-sibling::div/span/span");
	
	/**The btn Select Package Code*/
	By btnSelectPackageCode = By.xpath("//button[contains(@id,'SelectNew')]");
	
	/**The txt Search Package Code*/
	By txtSearchPackageCode = By.xpath("//div[text()='PackageCode']/../../preceding-sibling::tr/th/span/input");
	
	/**The btn New Package Select Popup Ok*/
	By btnNewPackageSelectPopupOk = By.xpath("//button[contains(@id,'MainPackgeSelect')]");
	
	/**The btn Package Change*/
	By btnPackageChange = By.xpath("//button[text()='Package Change']");
	
	/**The lbl Package Change Success Message*/
	By lblPackageChangeSuccessMsg = By.xpath("//div[contains(@id,'msgDlg')]/div//table/tbody/tr/td/div");
	
	/**The btn Success Popup OK*/
	By btnSuccessPopupOK = By.xpath("//button[text()='OK']");
	
	/**The btn Change Selection*/
	By btnChangeSelection = By.xpath("//button[text()='Change Selection']");
	
	/**The txt Search Connected Data Package Id*/
	By txtSearchConnectedPackageId = By.xpath("//div[text()='PackageId']/../../preceding-sibling::tr/th[2]/span/input");
	
	/**The btn Connected Package Popup Ok*/
	By btnConnectedPackagePopupOk = By.xpath("//button[contains(@id,'CloseAvailablePackagesPopup')]");
	
	/**The lst New Package Code*/
	String lstNewPackageCode = "//div[contains(@id,'db')]/table/tbody/tr/td/nobr[text()='%s']";
	
	/**The lst Existing Package Code*/
	String lstExistPackageCode = "//div[contains(@id,'db')]/table/tbody/tr/td/nobr//span[text()='%s']";
	
	/**The btn Select Dealer Sales Code*/
	By btnSelectDealerSalesCode = By.xpath("//label[text()='Dealer/Sales code']/../../..//button[text()='Select']");
	
	/**The txt Search Dealer Code*/
	By txtSearchDealerCode = By.xpath("//div[text()='AgentCode']/../../preceding-sibling::tr/th[1]/span/input");
	
	/**The lst Dealer Code*/
	String lstDealerCode = "//div[contains(@id,'db')]/table/tbody/tr/td/nobr[text()='%s']";
	
	/**The btn OK in Select Dealer Code*/
	By btnOkSelectDealerCode = By.xpath("//button[contains(@id,'CompleteSelectDealerCode')]");
	
	/** The txt pop up header. */
	By txtPopUpHeader=By.xpath("//a[@title='Close']/../../..//td[contains(@id,'msgDlg')]/div");
	
	/**The lbl List Existing Package Id*/
	By lblListExistingPackageId = By.xpath("//div[contains(@id,'db')]/table/tbody/tr/td[2]/nobr/span/span");
	
	/**The btn Back To Hotline*/
	By btnBackToHotline = By.xpath("//button[@accesskey='B']"); 

	
	/**
	 * Get Existing Package Code
	 * @author MalshaniS
	 *
	 * @return
	 */
	public String getExistingPackageCode(){
		return driver.findElement(lblPackageCode).getText();
	}
	
	/**
	 * Click Select Package Code
	 * @author MalshaniS
	 * @throws InterruptedException 
	 *
	 */
	public void clickSelectPackageCode() throws InterruptedException{
		Thread.sleep(3000);
		WebDriverWait wait = new WebDriverWait(driver, 600000);
		WebElement eleSelectPackageCode = wait.until(ExpectedConditions.visibilityOf(driver.findElement(btnSelectPackageCode)));
		eleSelectPackageCode.click();
	}
	
	/**
	 * Fill New Package Code
	 * @author MalshaniS
	 *
	 * @param packageCode
	 */
	public void fillNewPackageCode(String packageCode){
		WebDriverWait wait = new WebDriverWait(driver, 600000);
		WebElement eleSearchPackageCode = wait.until(ExpectedConditions.visibilityOf(driver.findElement(txtSearchPackageCode)));
		driver.findElement(txtSearchPackageCode).clear();
		eleSearchPackageCode.sendKeys(packageCode + "\n");
	}
	
	/**
	 * Select Package From List
	 * @author MalshaniS
	 *
	 * @param packageCode
	 */
	public void selectPackageCodeFromList(String packageCode){
		By lblNewPackageCode = By.xpath(String.format(lstNewPackageCode, packageCode));
		driver.findElement(lblNewPackageCode).click();
	}
	
	/**
	 * Select New Package
	 * @author MalshaniS
	 *
	 * @param packageCode
	 */
	public void selectNewPackage(String packageCode){
		fillNewPackageCode(packageCode);
		selectPackageCodeFromList(packageCode);
	}
	
	/**
	 * Click New Package List Popup Ok
	 * @author MalshaniS
	 *
	 */
	public void clickOkNewPackageList(){
		WebDriverWait wait = new WebDriverWait(driver, 600000);
		WebElement eleNewPackageSelectPopupOk = wait.until(ExpectedConditions.visibilityOf(driver.findElement(btnNewPackageSelectPopupOk)));
		eleNewPackageSelectPopupOk.click();
	}
	
	/**
	 * Click Package Change
	 * @author MalshaniS
	 * @throws InterruptedException 
	 *
	 */
	public void clickPackageChange() throws InterruptedException{
		Thread.sleep(4000);
		WebDriverWait wait = new WebDriverWait(driver, 800000);
		WebElement elePackageChange = wait.until(ExpectedConditions.visibilityOf(driver.findElement(btnPackageChange)));
		elePackageChange.click();
	}
	
	/**
	 * Get Package Change Success Message
	 * @author MalshaniS
	 *
	 * @return success Msg
	 */
	public String getPackageChangeSuccessMsg(){
		String message=null;
		List<String> lblPopupMassage = new ArrayList<>();
		
		WebDriverWait wait = new WebDriverWait(driver, 40000);
		WebElement elePopUpHeader = wait.until(ExpectedConditions.visibilityOf(driver.findElement(txtPopUpHeader)));
		
		List<WebElement> elePopupMessage = driver.findElements(lblPackageChangeSuccessMsg);
		
		for(int i=0; i<elePopupMessage.size(); i++){
			lblPopupMassage.add(i, elePopupMessage.get(i).getText().trim());
		}
				
		if(elePopUpHeader.getText().equalsIgnoreCase("Error")){		
			message = lblPopupMassage.get(0);
		}else{
			if(lblPopupMassage.size()>1){
				message = lblPopupMassage.get(1);
			}
			else{
				message = lblPopupMassage.get(0);
			}
		}	
		return message;
	}
	
	/**
	 * Click Success Popup OK
	 * @author MalshaniS
	 *
	 */
	public void clickSuccessPopupOk(){
		WebDriverWait wait = new WebDriverWait(driver, 500000);
		WebElement eleSuccessPopupOK = wait.until(ExpectedConditions.visibilityOf(driver.findElement(btnSuccessPopupOK)));
		eleSuccessPopupOK.click();
	}
	
	/**
	 * Click Change Selection
	 * @author MalshaniS
	 *
	 */
	public void clickChangeSelection(){
		WebDriverWait wait = new WebDriverWait(driver, 100000);
		WebElement eleChangeSelection = wait.until(ExpectedConditions.visibilityOf(driver.findElement(btnChangeSelection)));
		eleChangeSelection.click();
	}
	
	/**
	 * Fill Connected Data Package
	 * @author MalshaniS
	 *
	 * @param connectedDataPackage
	 */
	public void fillConnectedPackageCode(String connectedDataPackage){
		WebDriverWait wait = new WebDriverWait(driver, 100000);
		WebElement eleSearchConnectedPackageId = wait.until(ExpectedConditions.visibilityOf(driver.findElement(txtSearchConnectedPackageId)));
		driver.findElement(txtSearchConnectedPackageId).clear();
		eleSearchConnectedPackageId.sendKeys(connectedDataPackage + "\n");
	}
	
	/**
	 * Select Data Package From List
	 * @author MalshaniS
	 *
	 * @param connectedDataPackage
	 * @throws InterruptedException 
	 */
	public void selectConnectedPackageCode(String connectedDataPackage) throws InterruptedException{
		Thread.sleep(2000);
		By lblExistDataPackageId = By.xpath(String.format(lstExistPackageCode, connectedDataPackage));
		driver.findElement(lblExistDataPackageId).click();
	}
	
	/**
	 * Click Connected Package List Ok
	 * @author MalshaniS
	 *
	 */
	public void clickOkConnectedPackageList(){
		WebDriverWait wait = new WebDriverWait(driver, 100000);
		WebElement eleConnectedPackagePopupOk = wait.until(ExpectedConditions.visibilityOf(driver.findElement(btnConnectedPackagePopupOk)));
		eleConnectedPackagePopupOk.click();
	}
	
	/**
	 * Click Select Dealer Sales Code
	 * @author MalshaniS
	 * @throws InterruptedException 
	 *
	 */
	public void clickSelectDealerCode() throws InterruptedException{
		Thread.sleep(4000);
		WebDriverWait wait = new WebDriverWait(driver, 600000);
		WebElement eleSelectDealerSalesCode = wait.until(ExpectedConditions.visibilityOf(driver.findElement(btnSelectDealerSalesCode)));
		eleSelectDealerSalesCode.click();
	}
	
	/**
	 * Select Dealer Code
	 * @author MalshaniS
	 *
	 * @param agentId
	 * @throws InterruptedException 
	 */
	public void selectDealerSalesCode(String agentId) throws InterruptedException{
		fillDealerSaleCode(agentId);
		selectADealerSaleCodeFromList(agentId);
	}
	
	/**
	 * Fill Dealer Code
	 * @author MalshaniS
	 *
	 * @param agentId
	 */
	public void fillDealerSaleCode(String agentId){
		WebDriverWait wait = new WebDriverWait(driver, 600000);
		WebElement eleSearchDealerCode = wait.until(ExpectedConditions.visibilityOf(driver.findElement(txtSearchDealerCode)));
		driver.findElement(txtSearchDealerCode).clear();
		eleSearchDealerCode.sendKeys(agentId + "\n");
	}

	/**
	 * Select Dealer Code from List
	 * @author MalshaniS
	 *
	 * @param agentId
	 * @throws InterruptedException 
	 */
	public void selectADealerSaleCodeFromList(String agentId) throws InterruptedException{
		Thread.sleep(2000);
		By lblDealerCode = By.xpath(String.format(lstDealerCode, agentId));
		driver.findElement(lblDealerCode).click();
	}

	/**
	 * Click Ok in Dealer Code List
	 * @author MalshaniS
	 *
	 */
	public void clickOkDealerCodeList(){
		WebDriverWait wait = new WebDriverWait(driver, 100000);
		WebElement eleOkSelectDealerCode = wait.until(ExpectedConditions.visibilityOf(driver.findElement(btnOkSelectDealerCode)));
		eleOkSelectDealerCode.click();
	}
	
	/**
	 * Check is Existing Data Package Present
	 * @author MalshaniS
	 *
	 * @param existingDataPackage
	 * @return true, if data package exist
	 * @throws InterruptedException 
	 */
	public boolean isExistingDataPackagePresent(String existingDataPackage) throws InterruptedException{
		Thread.sleep(3000);
		boolean flag = false;
		List<WebElement> elePackageId = driver.findElements(lblListExistingPackageId);
		List<String> lstPackageId = new ArrayList<>();
		
		for(int i=0; i<elePackageId.size(); i++){
			lstPackageId.add(i, elePackageId.get(i).getText());
		}
		
		if(lstPackageId.contains(existingDataPackage)){
			flag = true;
		}
		return flag;
	}

	/**
	 * Click Back to Hotline
	 * @author MalshaniS
	 * @throws InterruptedException 
	 *
	 */
	public void clickBackToHotline() throws InterruptedException{
		Thread.sleep(2000);
		WebDriverWait wait = new WebDriverWait(driver, 100000);
		WebElement eleBackToHotline = wait.until(ExpectedConditions.visibilityOf(driver.findElement(btnBackToHotline)));
		eleBackToHotline.click();
	}
}
