package com.dialog.bulkAutomatedUpdate.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

// TODO: Auto-generated Javadoc
/**
 * The Class AgentBranchMaintenencePage.
 */
public class AgentBranchMaintenencePage {


	/** The driver. */
	WebDriver driver;
	
	/**
	 * Instantiates a new CRMMainHomePage page.
	 *
	 * @param driver the driver
	 */
	public AgentBranchMaintenencePage(WebDriver driver){
		this.driver = driver;
	}
	
	/** The txt agent code. */
	By txtAgentCode=By.xpath("//label[text()='Agent Code']/../../td[2]/input");
	
	/** The txt agent name. */
	By txtAgentName= By.xpath("//label[text()='Agent Name']/../../td[2]/input");
	
	/** The txt agent email. */
	By txtAgentEmail=By.xpath("//label[text()='Email']/../../td[2]/input");
	
	/** The btn Find. */
	By btnFind=By.xpath("//button[text()='Find']");
	
	/** The btn Execute. */
	By btnExecute=By.xpath("//button[text()='Execute']");
	
	/** The btn Save Changes. */
	By btnSaveChanges = By.xpath("//button[text()='Save Changes']");
	
	/** The btn Save Changes. */
	By btnReturnToDashboard = By.xpath("//button[text()='Return to Dashboard']");
	
	/**The lbl Popup Header*/
	By lblPopupHeader = By.xpath("//td[contains(@id,'msgDlg')]/div[text()='Error']");
	
	/**The lbl Popup Message*/
	By lblPopupMessage = By.xpath("//div[contains(@id,'msgDlg::_cnt')]//table/tbody/tr/td/div");
	
	/**The btn Popup OK*/
	By btnPopupOk = By.xpath("//button[contains(@id,'msgDlg::cancel')]");
	
	/**
	 * Gets the agent code.
	 *
	 * @return the agent code
	 */
	public String getAgentCode(){
		return driver.findElement(txtAgentCode).getAttribute("value");
	}
	
	/**
	 * Sets the agent code.
	 *
	 * @param agentCode the new agent code
	 * @throws InterruptedException 
	 */
	public void setAgentCode(String agentCode) throws InterruptedException{
		Thread.sleep(3000);
		driver.findElement(txtAgentCode).clear();
		driver.findElement(txtAgentCode).sendKeys(agentCode);
	}
	
	/**
	 * Gets the agent name.
	 *
	 * @return the agent name
	 */
	public String getAgentName(){
		return driver.findElement(txtAgentName).getAttribute("value").trim();
	}
	
	/**
	 * Sets the agent name.
	 *
	 * @param agentName the new agent name
	 * @throws InterruptedException 
	 */
	public void setAgentName(String agentName) throws InterruptedException{
		driver.findElement(txtAgentName).clear();
		driver.findElement(txtAgentName).sendKeys(agentName);
		Thread.sleep(1000);
	}
	
	/**
	 * Gets the agent email.
	 *
	 * @return the agent email
	 */
	public String getAgentEmail(){
		return driver.findElement(txtAgentEmail).getAttribute("value");
	}
	
	/**
	 * Sets the agent email.
	 *
	 * @param agentEmail the new agent email
	 */
	public void setAgentEmail(String agentEmail){
		driver.findElement(txtAgentEmail).clear();
		driver.findElement(txtAgentEmail).sendKeys(agentEmail);
	}
	
	public void clickFindBtn() throws InterruptedException{
		Thread.sleep(3000);
		WebDriverWait wait = new WebDriverWait(driver, 500000);
		WebElement eleFind = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(btnFind)));
		eleFind.click();
		Thread.sleep(1000);
	}
	
	public void clickExecuteBtn() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(driver, 500000);
		WebElement eleExecute = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(btnExecute)));
		eleExecute.click();
		Thread.sleep(3000);
	}
	
	/**
	 * Click Save Changes
	 * @author MalshaniS
	 * @throws InterruptedException 
	 *
	 */
	public void clickSaveChanges() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(driver, 500000);
		WebElement eleSaveChanges = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(btnSaveChanges)));
		eleSaveChanges.click();
		Thread.sleep(2000);
	}
	
	/**
	 * Click Return To Dashboard
	 * @author MalshaniS
	 * @throws InterruptedException 
	 *
	 */
	public void clickReturnToDashboard() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(driver, 500000);
		WebElement eleReturnToDashboard = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(btnReturnToDashboard)));
		eleReturnToDashboard.click();
		Thread.sleep(3000);
	}
	
	/**
	 * Verify Error Popup Displayed
	 * @author MalshaniS
	 *
	 * @return true is popup displayed
	 * @throws InterruptedException 
	 */
	public boolean isPopupDisplayed() throws InterruptedException{
		Thread.sleep(2000);
		boolean flag = false;
		try{
			if(driver.findElement(lblPopupHeader).isDisplayed()){
				flag = true;
			}
		}catch(NoSuchElementException e){
			flag = false;
		}
		return flag;
	}
	
	/**
	 * Get Popup Message
	 * @author MalshaniS
	 *
	 * @return popup Message
	 * @throws InterruptedException 
	 */
	public String getPopupMessage() throws InterruptedException{
		Thread.sleep(2000);
		String popupMessage = driver.findElement(lblPopupMessage).getText().trim();
		driver.findElement(btnPopupOk).click();
		return popupMessage;
	}
}
