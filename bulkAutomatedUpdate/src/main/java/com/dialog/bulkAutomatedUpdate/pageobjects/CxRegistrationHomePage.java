package com.dialog.bulkAutomatedUpdate.pageobjects;

import java.awt.AWTException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


// TODO: Auto-generated Javadoc
/**
 * The Class CxRegistrationHomePage.
 */
public class CxRegistrationHomePage {

	/** The driver. */
	WebDriver driver;
	
	/**
	 * Instantiates a new cx registration home page.
	 *
	 * @param driver the driver
	 */
	public CxRegistrationHomePage(WebDriver driver){
		this.driver=driver;
	}

	/**The txt Mobile No*/
	By txtMobileNo = By.xpath("//label[text()='Mobile No']/../following-sibling::td/span/input");
	
	/** The btn search. */
	By btnSearch=By.xpath("//span[text()='Search']");
	
	/**The dd Id Type*/
	By ddIdType = By.xpath("//label[text()='Identification']/../following-sibling::td/span/select");
	
	/**The txt Id Number*/
	By txtIdNumber = By.xpath("//input[contains(@id,'IdNm')]");
	
	/** The lnk select profile. */
	By lnkSelectProfile=By.xpath( "//a[text()='Select Profile']");
	
	/**The btn Edit Address*/
	By btnEditAddress = By.xpath("//button/span[text()='Edit Address']");
	
	/** The lbl profile not exist error message. */
	By lblProfileNotExistErrorMessage = By.xpath("//a[text()='Select Profile']/../../../../../following-sibling::td/table/tbody/tr/td/span");
	
	/** The txt customer address1. */
	By txtCustomerAddress1=By.xpath("//label[contains(text(),'Address Line 1')]/../following-sibling::td/input");
	
	/** The txt customer address2. */
	By txtCustomerAddress2=By.xpath( "//label[contains(text(),'Address Line 2')]/../following-sibling::td/input");
	
	/** The txt customer address3. */
	By txtCustomerAddress3=By.xpath("//label[contains(text(),'Address Line 3')]/../following-sibling::td/input");
	
	/**The txt Postal Code*/
	By txtPostalCode = By.xpath("//label[text()='Postal Code']/../following-sibling::td/span/input");
	
	/** The txt agent code. */
	By txtAgentCode=By.xpath("//label[contains(text(),'Agent Code')]/../following-sibling::td/span/input");
	
	/** The txt branch code. */
	By txtBranchCode=By.xpath("//label[contains(text(),'Branch Code')]/../following-sibling::td/span/input");
	
	/** The lbl registration success msg. */
	By lblRegistrationSuccessMsg=By.xpath("//div/table/tbody/tr/td/span/a");
	
	/** The btn save. */
	By btnSave=By.xpath("//span[text()='Save']");
	
	/** The btn agent drop down. */
	By btnAgentDropDown=By.xpath("//a[@title='Select Agent Code From The List']");
	
	/** The btn branch code drop down. */
	By btnBranchCodeDropDown=By.xpath("//a[@title='Select Branch Code From The List']");
	
	/** The lbl popup error. */
	By lblPopupError=By.xpath("//div[@id='d1::msgDlg::_cnt']/div/table[2]/tbody//div");
	
	/** The btn ok popup. */
	By btnOkPopup=By.xpath("//button[contains(@id,'msgDlg') and text()='OK']");
	
	/** The lbl registration success message content. */
	By lblRegistrationSuccessMessageContent=By.xpath("//span/textarea[contains(@id,'content')]");
	
	/** The link select profile. */
	By linkSelectProfile=By.xpath("//a[text()='Select Profile']");
	
	/** The tbl body profile. */
	By tblBodyProfile=By.xpath("//div[text()='Profiles List']/../../../../../../../tr[2]/td[2]/div/div/div/div[2]/table");
	
	/**The txt Sim*/
	String txtSim = "//span[contains(@id,'PhNo') and text()='%s']/../../../following-sibling::td/nobr/span[contains(@id,'Sim')]/input";
	
	/**The chk Active*/
	String chkBoxActive = "//span[contains(@id,'PhNo') and text()='%s']/../../../following-sibling::td/nobr//span/input[@type='checkbox']";
	
	/**The txt Credit Limit*/
	By txtCreditLimit = By.xpath("//label[text()='Credit Limit']/../following-sibling::td/input");
	
	/**The dd Credit Type*/
	By ddCreditType = By.xpath("//label[text()='Credit Type']/../following-sibling::td/span/input");
	
	/**The dd Bill Format*/
	By ddBillFormat = By.xpath("//label[text()='Bill Format']/../following-sibling::td/select");
	
	/**The dd Bill Dispatch Type*/
	By ddBillDispatchType = By.xpath("//label[text()='Bill Dispatch Type']/../following-sibling::td/select");
	
	/**The txt Hierarchy Account*/
	By txtHeirarchyAccount = By.xpath("//label[text()='Hierarchy Account']/../following-sibling::td/input");
	
	/**The txt Customer First Name*/
	By txtCustomerFirstName = By.xpath("//label[text()='First Name']/../following-sibling::td/input");
	
	/**The dd BR Code*/
	By ddBillRunCode = By.xpath("//label[text()='Bill Run Code']/../following-sibling::td/select");
	
	/**The Customer Category*/
	By ddCustomerCategory = By.xpath("//label[text()='Customer Category']/../following-sibling::td/span/input");
	
	/**The txt Sim*/
	String txtImsi = "//span[contains(@id,'PhNo') and text()='%s']/../../../following-sibling::td/nobr/span/label[text()='IMSI']/preceding-sibling::input";

	/**The txt Search Account Id*/
	By txtSearchAccountID = By.xpath("//div[contains(@id,'tblHierarchy')]/table/tbody/tr[2]/th[1]/span/input");
	
	/**The tbl Parent Node*/
	By tblParentNode = By.xpath("//div[contains(@id,'tblHierarchy::db')]/table");
	
	/**The btn Acc Infor OK*/
	By btnAccInforOk = By.xpath("//div[contains(@id,'blHierarchy')]/following-sibling::button[text()='OK']");
	
	/**The btn Search Postal Code*/
	By btnSearchPostalCode = By.xpath("//a[contains(@title,'Postal Code')]");
	
	/**The lnk Search*/
	By lnkSearch = By.xpath("//a[contains(.,'Search')]");
	
	/**The txt Create Address Postal Code*/
	By txtCreateAddressPostalCode = By.xpath("//label[text()='PostalCode']/../..//input");

	/**The btn Search*/
	By btnPostalCodeSearch = By.xpath("//button[text()='Search']");

	/**The ele Postal Code*/
//	String elePostalCode = "//div[contains(@id,'postalCodeId')]//table/tbody/tr/td/nobr[text()='%s']"; //Test Environment
	String elePostalCode = "//div[contains(@id,'SocPos_afrLovInternalTableId')]//table/tbody/tr/td[2]/nobr[text()='%s']"; //Live Environment
	
	/**The btn OK Postal Code List*/
	By btnOkPostalCodeList = By.xpath("//div[contains(@id,'SocPos')]//button[text()='OK']");
	
	/**The btn Reset*/
	By btnReset = By.xpath("//span[text()='Save']/../following-sibling::button/span[text()='Reset']");

	/**
	 * Select a Profile
	 *
	 * @param companyName
	 * @throws InterruptedException
	 * @throws AWTException 
	 */
	public void selectProfile(String companyName) throws InterruptedException, AWTException{
		if(driver.findElement(linkSelectProfile).isEnabled()==true){
			driver.findElement(linkSelectProfile).click();
			Thread.sleep(2000);
			int rowCount=0;
			int i=0;
			//Select a Profile		
			List<WebElement> row_tbl = driver.findElements(By.xpath("//div[contains(@id,'tblProf::db')]/table/tbody/tr"));
			rowCount = row_tbl.size();
		while (i==0){
			for(i=0; i<rowCount; i++){
				WebElement eleProfile = driver.findElement(By.xpath("//div[contains(@id,'tblProf::db')]/table/tbody/tr["+(i+1)+"]/td[4]/nobr"));
				JavascriptExecutor jse = ((JavascriptExecutor) driver);
				jse.executeScript("arguments[0].scrollIntoView(true);",eleProfile);
				Thread.sleep(1000);
				String firstName = eleProfile.getText();
				if (firstName.equalsIgnoreCase(companyName)) {
					eleProfile.click();
					Thread.sleep(2000);
					i++;
					break;
				}
			}
			if(i==rowCount){
				WebElement latProfile = driver.findElement(By.xpath("//div[contains(@id,'tblProf::db')]/table/tbody/tr["+(i-1)+"]/td[4]/nobr"));
				latProfile.click();
				Thread.sleep(3000);
				driver.findElement(linkSelectProfile).click();
				Thread.sleep(2000);
				i=0;
			}		
		}
		Thread.sleep(1000);
		}
	}
/*	public void selectProfile(String companyName) throws InterruptedException, AWTException{
		if(driver.findElement(linkSelectProfile).isEnabled()==true){
			driver.findElement(linkSelectProfile).click();
			Thread.sleep(2000);
			int rowCount=0;
			int i=0;
			//Select a Profile		
			List<WebElement> row_tbl = driver.findElements(By.xpath("//div[contains(@id,'tblProf::db')]/table/tbody/tr"));
			rowCount = row_tbl.size();
			
			while(i==0){
			for(i=0; i<rowCount; i++){
				WebElement eleProfile = driver.findElement(By.xpath("//div[contains(@id,'tblProf::db')]/table/tbody/tr["+(i+1)+"]/td[4]/nobr"));
				JavascriptExecutor jse = ((JavascriptExecutor) driver);
				jse.executeScript("arguments[0].scrollIntoView(true);",eleProfile);
				Thread.sleep(1000);
				String firstName = eleProfile.getText();
				if (firstName.equalsIgnoreCase(companyName)) {
					eleProfile.click();
					Thread.sleep(2000);
					i++;
					break;
				}
			}
			if(i==rowCount){
				WebElement latProfile = driver.findElement(By.xpath("//div[contains(@id,'tblProf::db')]/table/tbody/tr["+(i-1)+"]/td[4]/nobr"));
				latProfile.click();
				Thread.sleep(3000);
				driver.findElement(linkSelectProfile).click();
				Thread.sleep(2000);
				i=0;
			}		
		}
		
			if(rowCount>0){
				for(Iterator<WebElement> iterator = row_tbl.iterator(); iterator.hasNext();){
					WebElement cell;
					cell = iterator.next().findElement(By.xpath("td[4]/nobr"));
					JavascriptExecutor jse = ((JavascriptExecutor) driver);
					jse.executeScript("arguments[0].scrollIntoView(true);",cell);
					Thread.sleep(3000);
					String firstName = cell.getText();
					if (firstName.equalsIgnoreCase(companyName)) {
						cell.click();
						Thread.sleep(2000);
						break;
					}
				}
		Thread.sleep(1000);
		}
	}*/
	
	/**
	 * Fill Invoice No.
	 *
	 * @param invoiceNo the invoice no
	 */
	public void fillMobileNo(String mobileNo){
		driver.findElement(txtMobileNo).click();
		driver.findElement(txtMobileNo).clear();
		driver.findElement(txtMobileNo).sendKeys(mobileNo);		
	}
	
	/**
	 * Click search.
	 *
	 * @throws InterruptedException the interrupted exception
	 */
	public void clickSearch() throws InterruptedException{
		driver.findElement(btnSearch).click();
		Thread.sleep(4000);
	}
	
	/**
	 * Get Id Type
	 * @author MalshaniS
	 *
	 * @return idType
	 */
	public String getIdType(){
		return driver.findElement(ddIdType).getAttribute("title");
	}
	
	/**
	 * Get Id Number
	 * @author MalshaniS
	 *
	 * @return idNumber
	 */
	public String getIdNumber(){
		return driver.findElement(txtIdNumber).getAttribute("value");
	}
	
	/**
	 * Gets the profile not exist error.
	 *
	 * @return the profile not exist error
	 */
	public String getProfileNotExistError(){
		return driver.findElement(lblProfileNotExistErrorMessage).getText().trim();	 
	}
	
	public String getCustomerFirstName(){
		return driver.findElement(txtCustomerFirstName).getAttribute("value").trim();
	}
	
	/**
	 * Click btn Edit Address
	 * @author MalshaniS
	 * @throws InterruptedException 
	 *
	 */
	public void clickEditAddress() throws InterruptedException{
		driver.findElement(btnEditAddress).click();
		Thread.sleep(2000);
	}
	
	/**
	 * Get Address Line 1
	 * @author MalshaniS
	 *
	 * @return addressLine1
	 */
	public String getAddressLine1(){
		return driver.findElement(txtCustomerAddress1).getAttribute("value");
	}
	
	/**
	 * Get Address Line 2
	 * @author MalshaniS
	 *
	 * @return addressLine2
	 */
	public String getAddressLine2(){
		return driver.findElement(txtCustomerAddress2).getAttribute("value");
	}
	
	/**
	 * Get Address Line 3
	 * @author MalshaniS
	 *
	 * @return addressLine3
	 */
	public String getAddressLine3(){
		return driver.findElement(txtCustomerAddress3).getAttribute("value");
	}

	/**
	 * Get Postal Code
	 * @author MalshaniS
	 *
	 * @return PostalCode
	 */
	public String getPostalCode(){
		return driver.findElement(txtPostalCode).getAttribute("value");
	}

	/**
	 * Fill Address Line 1
	 * @author MalshaniS
	 *
	 * @param addressLine1
	 * @throws InterruptedException 
	 */
	public void fillAddressLine1(String addressLine1) throws InterruptedException{
		driver.findElement(txtCustomerAddress1).clear();
		driver.findElement(txtCustomerAddress1).sendKeys(addressLine1);
		Thread.sleep(500);
	}
	
	/**
	 * Fill Address Line 2
	 * @author MalshaniS
	 *
	 * @param addressLine2
	 * @throws InterruptedException 
	 */
	public void fillAddressLine2(String addressLine2) throws InterruptedException{
		driver.findElement(txtCustomerAddress2).clear();
		driver.findElement(txtCustomerAddress2).sendKeys(addressLine2);
		Thread.sleep(500);
	}
	
	/**
	 * Fill Address Line 3
	 * @author MalshaniS
	 *
	 * @param addressLine3
	 * @throws InterruptedException 
	 */
	public void fillAddressLine3(String addressLine3) throws InterruptedException{
		driver.findElement(txtCustomerAddress3).clear();
		driver.findElement(txtCustomerAddress3).sendKeys(addressLine3);
		Thread.sleep(500);
	}
	
	/**
	 * Enter Postal Code
	 * @author MalshaniS
	 *
	 * @param postalCode
	 */
	public void fillPostalCode(String postalCode){
		driver.findElement(txtPostalCode).clear();
		driver.findElement(txtPostalCode).sendKeys(postalCode + "\n");
	}
		
	/**
	 * Click save.
	 *
	 * @throws InterruptedException the interrupted exception
	 */
	public void clickSave() throws InterruptedException{
		driver.findElement(btnSave).click();	
		Thread.sleep(10000);
	}
	
	/**
	 * Gets the registration success message.
	 *
	 * @return the registration success message
	 * @throws InterruptedException the interrupted exception
	 */
	public String getRegistrationSuccessMessage() throws InterruptedException{
		Thread.sleep(1000);
		return driver.findElement(lblRegistrationSuccessMsg).getText();
	}
	
	/**
	 * Enter Sim number
	 * @author MalshaniS
	 *
	 * @param mobileNo
	 * @param simNo
	 * @throws InterruptedException
	 */
	public void enterConnectionDetails(String mobileNo, String simNo) throws InterruptedException{
			By txtSimNo = By.xpath(String.format(txtSim, mobileNo));
			driver.findElement(txtSimNo).sendKeys(simNo + "\t");
			Thread.sleep(2000);
			
			By chkActive = By.xpath(String.format(chkBoxActive, mobileNo));
			driver.findElement(chkActive).click();
			Thread.sleep(2000);
			
	} 
	
	/**
	 * Fill Credit Details
	 * @author MalshaniS
	 *
	 * @param creditLimit
	 * @param creditType
	 * @throws InterruptedException 
	 */
	public void fillCreditDetails(String creditLimit, String creditType, String customerCategory) throws InterruptedException{
			driver.findElement(txtCreditLimit).clear();
			driver.findElement(txtCreditLimit).sendKeys(creditLimit);
			Thread.sleep(1000);
			
			driver.findElement(ddCreditType).clear();
			driver.findElement(ddCreditType).sendKeys(creditType+"\n");
			Thread.sleep(1000);
			
			driver.findElement(ddCustomerCategory).clear();
			driver.findElement(ddCustomerCategory).sendKeys(customerCategory+"\n");
			Thread.sleep(1000);

			
	}
	
	/**
	 * Fill Billing Infor
	 * @author MalshaniS
	 *
	 * @param billFormat
	 * @param billDispatchType
	 * @throws InterruptedException 
	 */
	public void fillBillingInfor(String billFormat, String billDispatchType) throws InterruptedException{
		Select dropDownBillFormat=new Select(driver.findElement(ddBillFormat));
		dropDownBillFormat.selectByVisibleText(billFormat);
		
		Select dropDownBillDispatchType=new Select(driver.findElement(ddBillDispatchType));
		dropDownBillDispatchType.selectByVisibleText(billDispatchType);
		Thread.sleep(2000);
	}
	
	/**
	 * search Heirarchy Account
	 * @author MalshaniS
	 *
	 * @param parentNode
	 * @throws InterruptedException 
	 */
	public void searchHeirarchyAccount(String parentNode) throws InterruptedException{
		
		Actions action = new Actions(driver);
		action.doubleClick(driver.findElement(txtHeirarchyAccount)).perform();
		Thread.sleep(3000);
		driver.findElement(txtSearchAccountID).clear();
		Thread.sleep(4000);
		driver.findElement(txtSearchAccountID).sendKeys(parentNode+"\n");
		Thread.sleep(3000);
	}
	
	/**
	 * Check is Heirarchy Account Exist
	 * @author MalshaniS
	 *
	 * @return true; if Acc Exist
	 */
	public boolean isHieararchyAccountExist(){
		boolean flag = false;
		int rowCount = Integer.parseInt(driver.findElement(tblParentNode).getAttribute("_rowcount"));
		if(rowCount>=1){
			flag = true;
		}
		return flag;
	}
	
	/**
	 * Click OK
	 * @author MalshaniS
	 *
	 * @param parentNode
	 * @throws InterruptedException
	 */
	public void selectHeirarchyAccount() throws InterruptedException{
		driver.findElement(btnAccInforOk).click();
		Thread.sleep(1000);
	}
	
	/**
	 * Enter branch code.
	 *
	 * @param branchCode the branch code
	 * @throws InterruptedException the interrupted exception
	 */
	public void enterBranchCode(String branchCode) throws InterruptedException{
		driver.findElement(txtBranchCode).sendKeys(branchCode+"\n");
		Thread.sleep(6000);
	}
	
	/**
	 * Enter agent code.
	 *
	 * @param agentCode the agent code
	 * @throws InterruptedException the interrupted exception
	 */
	public void enterAgentCode(String agentCode) throws InterruptedException{
		driver.findElement(txtAgentCode).sendKeys(agentCode+"\n");
		Thread.sleep(6000);
	}
	
	/**
	 * Gets the success msg content.
	 *
	 * @return the success msg content
	 * @throws InterruptedException the interrupted exception
	 */
	public String getMsgContent() throws InterruptedException{
		driver.findElement(lblRegistrationSuccessMsg).click();
		Thread.sleep(2000);
		return driver.findElement(lblRegistrationSuccessMessageContent).getText().trim();
	}
	
	public boolean isExistingProfile(){
		boolean flag = false;
		if(driver.findElement(linkSelectProfile).isEnabled()){
			flag = true;
		}
		return flag;
	}
	
	/**
	 * Get Bill run Code
	 * @author MalshaniS
	 *
	 * @return billruncode
	 */
	public String getBillRunCode(){
		return driver.findElement(ddBillRunCode).getAttribute("title").trim();
	}
	
	/**
	 * Get IMSI no
	 * @author MalshaniS
	 *
	 * @return imsiNo
	 */
	public String getImsi(String mobileNo){
		By txtImsiNo = By.xpath(String.format(txtImsi, mobileNo));
		return driver.findElement(txtImsiNo).getAttribute("value");
	}
	
	/**
	 * Select Postal Code
	 * @author MalshaniS
	 *
	 * @param postalCode
	 * @throws InterruptedException 
	 */
	public void selectNewPostalCode(String postalCode) throws InterruptedException{
		Thread.sleep(3000);
		driver.findElement(btnSearchPostalCode).click();
		Thread.sleep(2000);

		WebDriverWait wait = new WebDriverWait(driver, 600000);
		WebElement eleSearch = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(lnkSearch)));
		eleSearch.click();
		
		Thread.sleep(2000);
		driver.findElement(txtCreateAddressPostalCode).sendKeys(postalCode);
		driver.findElement(btnPostalCodeSearch).click();
		Thread.sleep(2000);
		
		By lblPostalCode = By.xpath(String.format(elePostalCode, postalCode));
		driver.findElement(lblPostalCode).click();
		Thread.sleep(2000);
		
		driver.findElement(btnOkPostalCodeList).click();
	
//		driver.findElement(txtPostalCode).clear();
//		driver.findElement(txtPostalCode).sendKeys(postalCode+"\t");
	}
	
	/**
	 * Select Bill Run Code
	 * @author MalshaniS
	 *
	 * @param billRunCode
	 */
	public void selectBillRunCode(String billRunCode){
		Select dropDownBillRunCode=new Select(driver.findElement(ddBillRunCode));
		dropDownBillRunCode.selectByVisibleText(billRunCode);
	}
	
	public void clickReset() throws InterruptedException{
		driver.findElement(btnReset).click();
		Thread.sleep(1000);
	}

}
