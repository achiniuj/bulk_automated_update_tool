package com.dialog.bulkAutomatedUpdate.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * 
 * The Class CCBSContractUpdatePage
 *
 */
public class CCBSContractUpdatePage {
	
	/**
	 * The driver
	 */
	WebDriver driver;
	
	/**
	 * Instantiates a new Contract Update page.
	 * @param driver 
	 */
	public CCBSContractUpdatePage(WebDriver driver){
		this.driver = driver;
	}

	/**The txt Credit Limit*/
	By txtCreditLimit = By.xpath("//label[text()='Credit Limit']/../..//input");
	
	/**The dd Customer Category*/
	By ddCustomerCategory = By.xpath("//label[text()='Customer Category']/../..//select");
	
	/**The dd Credit Type*/
	By ddCreditType = By.xpath("//label[text()='Credit Type']/../..//select");
	
	/**The btn Save Changes*/
	By btnSaveChanges = By.xpath("//button[@accesskey='S']");
	
	/**The lbl Attribute Change Msg*/
	By lblAttributeChangeMsg = By.xpath("//button[@accesskey='B']/../..//span[contains(@id,'content')]");
	
	/**The btn Back To Hotline*/
	By btnBackToHotline = By.xpath("//button[@accesskey='S']/../following-sibling::td/button[@accesskey='B']");
	
	
	/**
	 * Fill Credit Limit
	 * @author MalshaniS
	 *
	 * @param creditLimit
	 * @throws InterruptedException 
	 */
	public void fillCreditLimit(String creditLimit) throws InterruptedException{
		Thread.sleep(5000);
		WebDriverWait wait = new WebDriverWait(driver, 500000);
		WebElement eleCreditLimit = wait.until(ExpectedConditions.visibilityOf(driver.findElement(txtCreditLimit)));
		eleCreditLimit.clear();
		eleCreditLimit.sendKeys(creditLimit);
	}
	
	/**
	 * Select Customer Category
	 * @author MalshaniS
	 *
	 * @param customerCategory
	 */
	public void selectCustomerCategory(String customerCategory){
		Select objSelect = new Select(driver.findElement(ddCustomerCategory));
		objSelect.selectByVisibleText(customerCategory);
	}
	
	/**
	 * Select Credit Type
	 * @author MalshaniS
	 *
	 * @param creditType
	 */
	public void selectCreditType(String creditType){
		Select objSelect = new Select(driver.findElement(ddCreditType));
		objSelect.selectByVisibleText(creditType);
	}
	
	/**
	 * Click Save Changes
	 * @author MalshaniS
	 * @throws InterruptedException 
	 *
	 */
	public void clickSaveChanges() throws InterruptedException{
		Thread.sleep(1000);
		WebDriverWait wait = new WebDriverWait(driver, 600000);
		WebElement eleSaveChanges = wait.until(ExpectedConditions.visibilityOf(driver.findElement(btnSaveChanges)));
		eleSaveChanges.click();
	}
	
	/**
	 * GetAttribute Change Message
	 * @author MalshaniS
	 *
	 * @return attributeChangeMsg
	 * @throws InterruptedException 
	 */
	public String getAttributeChangeMessage() throws InterruptedException{
		Thread.sleep(5000);
		WebDriverWait wait = new WebDriverWait(driver, 600000);
		WebElement eleAttributeChangeMsg = wait.until(ExpectedConditions.visibilityOf(driver.findElement(lblAttributeChangeMsg)));
		return eleAttributeChangeMsg.getText();
	}
	
	/**
	 * Get Credit Limit
	 * @author MalshaniS
	 * @return credit limit
	 * @throws InterruptedException 
	 *
	 */
	public String getCreditLimit() throws InterruptedException{
		WebDriverWait wait = new WebDriverWait(driver, 500000);
		WebElement eleCreditLimit = wait.until(ExpectedConditions.visibilityOf(driver.findElement(txtCreditLimit)));
		return eleCreditLimit.getAttribute("value");
	}

	/**
	 * Get Credit Type
	 * @author MalshaniS
	 *
	 * @return credit type
	 */
	public String getCreditType(){
		return driver.findElement(ddCreditType).getAttribute("title");
	}

	/**
	 * Get Customer Category
	 * @author MalshaniS
	 *
	 * @return customer category
	 */
	public String getCustomerCategory(){
		return driver.findElement(ddCustomerCategory).getAttribute("title");
	}
	
	/**
	 * Click Back to Hotline
	 * @author MalshaniS
	 *
	 */
	public void clickBackToHotline(){
		driver.findElement(btnBackToHotline).click();
	}
}
