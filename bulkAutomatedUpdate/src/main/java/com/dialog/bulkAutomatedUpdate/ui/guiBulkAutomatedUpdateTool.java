package com.dialog.bulkAutomatedUpdate.ui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.io.FilenameUtils;
import org.testng.TestNG;
import org.testng.xml.Parser;
import org.testng.xml.XmlSuite;
import org.xml.sax.SAXException;

import com.dialog.bulkAutomatedUpdate.base.ReadPropertyFileData;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

import javax.swing.ButtonGroup;
import java.awt.event.InputMethodListener;
import java.awt.event.InputMethodEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JPasswordField;

public class guiBulkAutomatedUpdateTool extends JFrame {
	public guiBulkAutomatedUpdateTool() throws IOException {
		setTitle("Dialog BAU");
		this.toolUi();
	}

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtUserName;
	private JTextField txtTo;
	private JTextField txtCc;
	private JTextField txtBcc;
	private JTextField txtUploadDataSheet;
	
	public ReadPropertyFileData proFile = new ReadPropertyFileData();
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JPasswordField txtPassword;
	
	/**
	 * Launch the application.
	 */
	public void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
//					guiBulkAutomatedUpdateTool frame = new guiBulkAutomatedUpdateTool();
//					frame.setVisible(true);
					toolUi();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame
	 * @throws IOException 
	 */
//	public guiBulkAutomatedUpdateTool() throws IOException {
	public void toolUi() throws IOException {
		
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 513, 309);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		//Tab Panel
		final JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		//Tab_1: Panel Data Configurations
		JPanel pnlConfigurations = new JPanel();
		pnlConfigurations.setBorder(null);
		tabbedPane.addTab("Data Configurations", null, pnlConfigurations, null);
		pnlConfigurations.setLayout(null);
		
		//Tab_2: Panel Login Details
		JPanel pnlLoginDetails = new JPanel();
		tabbedPane.addTab("Application Login Details", null, pnlLoginDetails, null);
		tabbedPane.setEnabledAt(1, false);
		pnlLoginDetails.setLayout(null);
		
		//Tab_3: Panel Email Configuration
		JPanel pnlEmailConfiguration = new JPanel();
		tabbedPane.addTab("Email Configuration", null, pnlEmailConfiguration, null);
		tabbedPane.setEnabledAt(2, false);
		pnlEmailConfiguration.setLayout(null);
		
		//Tab_1:
		//Upload Data Sheet		
		JLabel lblUploadDataSheet = new JLabel("Upload Data Sheet");
		lblUploadDataSheet.setBounds(10, 38, 128, 17);
		pnlConfigurations.add(lblUploadDataSheet);
		
		final JLabel lblUploadSuccessMessage = new JLabel("");
		lblUploadSuccessMessage.setBounds(148, 66, 238, 14);
		pnlConfigurations.add(lblUploadSuccessMessage);
		
		JButton btnUpload = new JButton("Upload");		
		btnUpload.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnUpload.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				//Choose File From Location
				JFileChooser fileChooser = new JFileChooser();
				File file = null;
				fileChooser.setCurrentDirectory(new File("C:\\bulkAutomatedUpdate\\"));
				int returnValue = fileChooser.showOpenDialog(guiBulkAutomatedUpdateTool.this);
				if(returnValue == JFileChooser.APPROVE_OPTION){
					file = fileChooser.getSelectedFile();
					txtUploadDataSheet.setText(file.getName());	
					
				}else{
					lblUploadSuccessMessage.setForeground(Color.RED);
					lblUploadSuccessMessage.setText("File Access Cancelled by User.");
				}	
				
				if(file.getName().contains("Bulk_Data_Sheet.xlsx")){
					//Save file to specified location
					Path folder = Paths.get("C:\\bulkAutomatedUpdate\\");
					String fileName = FilenameUtils.getBaseName(file.getName());
					String fileExtension = FilenameUtils.getExtension(file.getName());
					try {
						Path tempFilePath = Paths.get(file.getAbsolutePath());
						Path filePath = Paths.get(folder + "\\" , fileName + "." + fileExtension);
						Files.copy(tempFilePath, filePath, StandardCopyOption.REPLACE_EXISTING);
						lblUploadSuccessMessage.setForeground(Color.GREEN);
						lblUploadSuccessMessage.setText("Data Sheet Uploaded Successfully.");
					
					} catch (IOException e1) {
						lblUploadSuccessMessage.setForeground(Color.RED);
						lblUploadSuccessMessage.setText("Data Sheet Not Uploaded Successfully.");
						e1.printStackTrace();
					}
				}else{
					lblUploadSuccessMessage.setForeground(Color.RED);
					lblUploadSuccessMessage.setText("Incorrect Data File.");
				}
			}
		});
		btnUpload.setBounds(340, 32, 89, 23);
		pnlConfigurations.add(btnUpload);
		
		txtUploadDataSheet = new JTextField();
		txtUploadDataSheet.setBounds(148, 33, 182, 20);
		pnlConfigurations.add(txtUploadDataSheet);
		txtUploadDataSheet.setColumns(10);
		
		//Select Bulk Update Scenario
		final JLabel lblBulkUpdateScenario = new JLabel("Bulk Update Scenario");
		lblBulkUpdateScenario.setBounds(10, 95, 128, 17);
		pnlConfigurations.add(lblBulkUpdateScenario);
		
		final JLabel lblErrorSelectScenario = new JLabel("");
		lblErrorSelectScenario.setBounds(340, 96, 184, 14);
		pnlConfigurations.add(lblErrorSelectScenario);
		
		final JComboBox<String> cmbScenarios = new JComboBox<String>();
		cmbScenarios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblErrorSelectScenario.setText("");
				String scenario = (String) cmbScenarios.getSelectedItem();
				try {
					proFile.setPropertyValue("sheetName", scenario);
				} catch (ConfigurationException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});	
		cmbScenarios.setModel(new DefaultComboBoxModel<String>(new String[] {"None", "Change_Of_Profile_Name", "Change_Of_BillingAddress_Name", "Change_Of_PR_Name", "Change_Of_ID", "Change_Of_Credit_Limit", "Change_Of_Billing_Address", "Change_Of_Credit_Type", "Change_Of_Credit_Category", "Change_Of_Cx_Segment", "Change_Of_Language", "Change_Of_DOB_DOR", "Change_Of_Bill_Dispatch_Type", "Change_Of_Email", "Change_Of_Ebill_Email", "Change_Of_ContactPerson", "Change_Of_Main_Package", "Change_Of_AccountManagerDetails", "Change_Of_Data_Package", "M2M_Migration"}));
		cmbScenarios.setBounds(148, 93, 182, 20);
		pnlConfigurations.add(cmbScenarios);
			
		//Choose Verify Existing Values
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(null);
		panel_1.setBounds(148, 132, 129, 36);
		pnlConfigurations.add(panel_1);
		panel_1.setLayout(null);
		
		final JLabel lblErrorSelectValue = new JLabel("");
		lblErrorSelectValue.setBounds(340, 143, 184, 14);
		pnlConfigurations.add(lblErrorSelectValue);
		
		final JLabel lblVerifyExistingValues = new JLabel("Verify Existing Values");
		lblVerifyExistingValues.setBounds(10, 135, 128, 22);
		pnlConfigurations.add(lblVerifyExistingValues);
		
		final JRadioButton rdbYes = new JRadioButton("Yes");
		rdbYes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					lblErrorSelectValue.setText("");
					proFile.setPropertyValue("verifyOldVlaue", "Y");
				} catch (ConfigurationException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		buttonGroup.add(rdbYes);
		rdbYes.setBounds(6, 7, 57, 23);
		panel_1.add(rdbYes);
		
		final JRadioButton rdbNo = new JRadioButton("No");
		rdbNo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					lblErrorSelectValue.setText("");
					proFile.setPropertyValue("verifyOldVlaue", "N");
				} catch (ConfigurationException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});		
		buttonGroup.add(rdbNo);
		rdbNo.setBounds(65, 7, 57, 23);
		panel_1.add(rdbNo);
		
		//Click Next Button
		JButton btnNext1 = new JButton("Next");
		btnNext1.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent arg0) {
			if(txtUploadDataSheet.getText().isEmpty()){
				lblUploadSuccessMessage.setForeground(Color.RED);
				lblUploadSuccessMessage.setText("Please Upload Data Sheet.");
			}
			if(cmbScenarios.getSelectedItem().toString().equalsIgnoreCase("None")){
				lblErrorSelectScenario.setForeground(Color.RED);
				lblErrorSelectScenario.setText("Please Select Scenario.");
			}
			if(!rdbYes.isSelected()){
				if(!rdbNo.isSelected()){
					lblErrorSelectValue.setForeground(Color.RED);
					lblErrorSelectValue.setText("Please Select a value.");
				}
			}
			if(!txtUploadDataSheet.getText().isEmpty() && lblErrorSelectScenario.getText().isEmpty() && lblErrorSelectValue.getText().isEmpty()){
				tabbedPane.setSelectedIndex(1);
				tabbedPane.setEnabledAt(1, true);
			}
		}
		});
		btnNext1.setBounds(340, 198, 89, 23);
		pnlConfigurations.add(btnNext1);
		
		//Tab_2:
		//Enter User Name
		JLabel lblUserName = new JLabel("User Name");
		lblUserName.setBounds(10, 47, 79, 14);
		pnlLoginDetails.add(lblUserName);
		
		final JLabel lblErrorEmptyUserName = new JLabel("");
		lblErrorEmptyUserName.setBounds(227, 47, 182, 14);
		pnlLoginDetails.add(lblErrorEmptyUserName);
		
		txtUserName = new JTextField();
		txtUserName.addInputMethodListener(new InputMethodListener() {
			public void caretPositionChanged(InputMethodEvent arg0) {
			}
			public void inputMethodTextChanged(InputMethodEvent arg0) {
				lblErrorEmptyUserName.setText("");
			}
		});
		txtUserName.setBounds(97, 44, 120, 20);
		pnlLoginDetails.add(txtUserName);
		txtUserName.setColumns(10);
		
		//Enter Password
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(10, 89, 79, 14);
		pnlLoginDetails.add(lblPassword);
		
		final JLabel lblErrorEmptyPassword = new JLabel("");
		lblErrorEmptyPassword.setBounds(227, 89, 182, 14);
		pnlLoginDetails.add(lblErrorEmptyPassword);
		
		txtPassword = new JPasswordField();
		txtPassword.addInputMethodListener(new InputMethodListener() {
			public void caretPositionChanged(InputMethodEvent event) {
			}
			public void inputMethodTextChanged(InputMethodEvent event) {
				lblErrorEmptyPassword.setText("");
			}
		});
		txtPassword.setBounds(97, 86, 120, 20);
		pnlLoginDetails.add(txtPassword);
		
		//Click Next Button
		JButton btnNext2 = new JButton("Next");
		btnNext2.addMouseListener(new MouseAdapter() {
			@SuppressWarnings("deprecation")
			@Override
			public void mouseClicked(MouseEvent e) {
				if(txtUserName.getText().isEmpty()){
					lblErrorEmptyUserName.setForeground(Color.RED);
					lblErrorEmptyUserName.setText("Please Enter Username.");
				}
				if(txtPassword.getText().isEmpty()){
					lblErrorEmptyPassword.setForeground(Color.RED);
					lblErrorEmptyPassword.setText("Please Enter Password.");
				}
				if(lblErrorEmptyUserName.getText().isEmpty() && lblErrorEmptyPassword.getText().isEmpty()){
					String userName = txtUserName.getText();
					String password = txtPassword.getText();
					try {
						proFile.setPropertyValue("defaultUser", userName);
					} catch (ConfigurationException | IOException e1) {
						e1.printStackTrace();
					}
					try {
						proFile.setPropertyValue("defaultPassword", password);
					} catch (ConfigurationException | IOException e1) {
						e1.printStackTrace();
					}
					
					tabbedPane.setSelectedIndex(2);
					tabbedPane.setEnabledAt(2, true);
				}
			}
		});
		btnNext2.setBounds(320, 190, 89, 23);
		pnlLoginDetails.add(btnNext2);
		
		//Tab_3:
		//Configure Email
		JLabel lblConfigureEmail = new JLabel("Send Execution Reports via Email : ");
		lblConfigureEmail.setBounds(6, 22, 174, 14);
		pnlEmailConfiguration.add(lblConfigureEmail);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(null);
		panel_2.setBounds(190, 11, 142, 38);
		pnlEmailConfiguration.add(panel_2);
		panel_2.setLayout(null);
		
		JRadioButton rdbtnYes = new JRadioButton("Yes");
		rdbtnYes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					txtTo.setEditable(true);
					txtCc.setEditable(true);
					txtBcc.setEditable(true);
					proFile.setPropertyValue("configure.email", "true");
				} catch (ConfigurationException ex) {
					ex.printStackTrace();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		});
		buttonGroup.add(rdbtnYes);
		rdbtnYes.setBounds(6, 7, 57, 23);
		panel_2.add(rdbtnYes);
		
		JRadioButton rdbtnNo = new JRadioButton("No");
		rdbtnNo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					txtTo.setEditable(false);
					txtCc.setEditable(false);
					txtBcc.setEditable(false);
					proFile.setPropertyValue("configure.email", "false");
					
				} catch (ConfigurationException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		buttonGroup.add(rdbtnNo);
		rdbtnNo.setBounds(79, 7, 57, 23);
		panel_2.add(rdbtnNo);
		
		//Enter Email Recipients
		JPanel panel = new JPanel();
		panel.setBorder(null);
		panel.setForeground(Color.WHITE);
		panel.setBounds(10, 59, 322, 95);
		pnlEmailConfiguration.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("TO : ");
		lblNewLabel.setBounds(10, 11, 34, 14);
		panel.add(lblNewLabel);
		
		txtTo = new JTextField();
		txtTo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		txtTo.setEditable(false);
		txtTo.setBounds(54, 8, 209, 20);
		panel.add(txtTo);
		txtTo.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("CC : ");
		lblNewLabel_1.setBounds(10, 40, 34, 14);
		panel.add(lblNewLabel_1);
		
		txtCc = new JTextField();
		txtCc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		txtCc.setEditable(false);
		txtCc.setBounds(54, 37, 209, 20);
		panel.add(txtCc);
		txtCc.setColumns(10);
		
		JLabel lblBcc = new JLabel("BCC : ");
		lblBcc.setBounds(10, 71, 34, 14);
		panel.add(lblBcc);
		
		txtBcc = new JTextField();
		txtBcc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		txtBcc.setEditable(false);
		txtBcc.setBounds(54, 68, 209, 20);
		panel.add(txtBcc);
		txtBcc.setColumns(10);
		
		//Click Start
		final JButton btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String to = txtTo.getText();
				try {
					proFile.setPropertyValue("email.sendTo", to);
				} catch (ConfigurationException | IOException e1) {
					e1.printStackTrace();
				}

				String cc = txtCc.getText();
				try {
					proFile.setPropertyValue("email.sendCC", cc);
				} catch (ConfigurationException | IOException e1) {
					e1.printStackTrace();
				}

				String bcc = txtBcc.getText();
				try {
					proFile.setPropertyValue("email.sendBCC", bcc);
				} catch (ConfigurationException | IOException e1) {
					e1.printStackTrace();
				}

				new Thread(){
					
					public void run(){
						System.out.print("starting thread");
						String xmlFileName = "testng.xml";
						List<XmlSuite> suite;
						try
						{
							TestNG testng = new TestNG();
							suite = (List <XmlSuite>)(new Parser(xmlFileName).parse());
							testng.setXmlSuites(suite);
							testng.run();
						}
						catch (ParserConfigurationException ex)
						{
							ex.printStackTrace();
						}
						catch (SAXException ex)
						{
							ex.printStackTrace();
						}
						catch (IOException ex)
						{
							ex.printStackTrace();
						}
					 }
				}.start();
				
				//disabling button to prevent concurrent starts
				btnStart.setEnabled(false);;
			}
		});
		btnStart.setBounds(284, 199, 89, 23);
		pnlEmailConfiguration.add(btnStart);
		
		final JButton btnStop = new JButton("Stop");
		btnStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnStop.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0){		
			System.exit(0);
		}
	});
		btnStop.setBounds(383, 199, 89, 23);
		pnlEmailConfiguration.add(btnStop);

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				System.exit(0);
			}
		});

	}
}
