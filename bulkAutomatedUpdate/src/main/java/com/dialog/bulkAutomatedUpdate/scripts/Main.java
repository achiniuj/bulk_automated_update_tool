package com.dialog.bulkAutomatedUpdate.scripts;

import java.io.IOException;

import com.dialog.bulkAutomatedUpdate.ui.guiBulkAutomatedUpdateTool;

// TODO: Auto-generated Javadoc
/**
 * The Class Main.
 * @author AchiniUj
 * Class contain the main method
 */
public class Main {
	/**
	 * The main method.
	 *
	 * @param ar the arguments
	 * @throws IOException 
	 */
	public static void main(String ar[]) throws IOException{
		guiBulkAutomatedUpdateTool ui = new guiBulkAutomatedUpdateTool();
		ui.main(ar);	 
	}
}
