package com.dialog.bulkAutomatedUpdate.scripts;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Hashtable;
import javax.mail.MessagingException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.dialog.bulkAutomatedUpdate.base.EmailSender;
import com.dialog.bulkAutomatedUpdate.base.ExcelUtils;
import com.dialog.bulkAutomatedUpdate.base.OpenWebDriver;
import com.dialog.bulkAutomatedUpdate.base.ReadPropertyFileData;
import com.dialog.bulkAutomatedUpdate.dataContainer.FrontEndStringsContainer.StringsAccountUpdatePage;
import com.dialog.bulkAutomatedUpdate.dataContainer.FrontEndStringsContainer.StringsAddressChangePage;
import com.dialog.bulkAutomatedUpdate.dataContainer.FrontEndStringsContainer.StringsContractUpdatePage;
import com.dialog.bulkAutomatedUpdate.dataContainer.FrontEndStringsContainer.StringsEbillConfirmationPage;
import com.dialog.bulkAutomatedUpdate.dataContainer.FrontEndStringsContainer.StringsPackageChangePage;
import com.dialog.bulkAutomatedUpdate.dataContainer.FrontEndStringsContainer.StringsSubscriberProfileUpdatePage;
import com.dialog.bulkAutomatedUpdate.pageobjects.AgentBranchMaintenencePage;
import com.dialog.bulkAutomatedUpdate.pageobjects.CCBSAccountUpdatePage;
import com.dialog.bulkAutomatedUpdate.pageobjects.CCBSAddressPage;
import com.dialog.bulkAutomatedUpdate.pageobjects.CCBSContractUpdatePage;
import com.dialog.bulkAutomatedUpdate.pageobjects.CCBSEbillConfirmationPage;
import com.dialog.bulkAutomatedUpdate.pageobjects.CCBSHomePage;
import com.dialog.bulkAutomatedUpdate.pageobjects.CCBSLoginPage;
import com.dialog.bulkAutomatedUpdate.pageobjects.CCBSPackageChangePage;
import com.dialog.bulkAutomatedUpdate.pageobjects.CCBSSubscriberProfilesUpdatePage;
import com.dialog.bulkAutomatedUpdate.pageobjects.CRMLoginPage;
import com.dialog.bulkAutomatedUpdate.pageobjects.CRMMainHomePage;
import com.dialog.bulkAutomatedUpdate.pageobjects.CxRegistrationHomePage;
import com.dialog.bulkAutomatedUpdate.pageobjects.CxRegistrationLoginPage;


public class UpdateScript {

	private OpenWebDriver openwebdriver;
	
	private CCBSLoginPage ccbsLoginPage;
	
	private CCBSHomePage ccbsHomePage;
	
	private CCBSSubscriberProfilesUpdatePage subscriberProfilePage;
	
	private CCBSAccountUpdatePage accountUpdatePage;
	
	private CCBSAddressPage addressPage;
	
	private CCBSContractUpdatePage contractUpdatePage;
	
	private CCBSEbillConfirmationPage ebillConfirmationPage;
	
	private CCBSPackageChangePage packageChange;
	
	private CRMMainHomePage CRMHomepage;
	
	private CRMLoginPage crmLoginPage;
	
	private CxRegistrationLoginPage cxRegPage;
	
	//private EmailSender sendEmail;
	
	private AgentBranchMaintenencePage agentMasterPage;
	
	private CxRegistrationHomePage cxRegistrationsHomePage;
	
	public ReadPropertyFileData propFile = new ReadPropertyFileData();
	
	public WebDriver driver;
	public String sheetName;
	public String mobileNo = null;
	public String accountId = null;
	public String agentCode = null;
	public String invoiceNo = null;

	@BeforeClass
	public void loginToApplication() throws Exception{
		sheetName = propFile.getPropertyValue("sheetName");
		if(sheetName.equalsIgnoreCase("Change_Of_Installation_Address")){
			loginToCRMMainPage();			
		}
		else if(sheetName.equalsIgnoreCase("Change_Of_AccountManagerDetails")){
			loginToCRMMainPage();
			CRMHomepage=new CRMMainHomePage(driver);
			CRMHomepage.crmMainMenuNavigation("Master Forms");
			CRMHomepage.crmSubMenuNavigation("Agent/Branch Maintenence");
		}
		else if (sheetName.equalsIgnoreCase("M2M_Migration")){
			loginToCRMMainPage();
			CRMHomepage=new CRMMainHomePage(driver);
			CRMHomepage.crmMainMenuNavigation("Cx Registration");
			CRMHomepage.crmSubMenuNavigation("GSM Registration");
		    for(String winHandle:driver.getWindowHandles()){
		        driver.switchTo().window(winHandle); 
		    }
			
//			loginToCxRegistrationPage();		
		}
		else{
			loginToOpenHotline();
		}
	}
	
	@Test(priority=1,dataProvider = "testData", dataProviderClass = com.dialog.bulkAutomatedUpdate.base.ExcelDataProvider.class)
	public void main (Hashtable<String, String> data) throws Exception{	
		
		if(data.get("Flag").equalsIgnoreCase("NOT_EXECUTED")||data.get("Flag").equalsIgnoreCase("")){
		
		if(driver==null){
			loginToApplication();
		}
		
		String filePath = propFile.getPropertyValue("dataFilePath");
		String errorMessage = null;
		try{
			
		int sheetNo = 0;
		if (sheetName.equalsIgnoreCase("Change_Of_Profile_Name")){
			sheetNo=1;
		}else if(sheetName.equalsIgnoreCase("Change_Of_BillingAddress_Name")){
			sheetNo=2;
		}else if(sheetName.equalsIgnoreCase("Change_Of_PR_Name")){
			sheetNo=3;
		}else if(sheetName.equalsIgnoreCase("Change_Of_ID")){
			sheetNo=4;
		}else if(sheetName.equalsIgnoreCase("Change_Of_Credit_Limit")){
			sheetNo=5;
		}else if(sheetName.equalsIgnoreCase("Change_Of_Billing_Address")){
			sheetNo=6;
		}else if(sheetName.equalsIgnoreCase("Change_Of_Credit_Type")){
			sheetNo=7;
		}else if(sheetName.equalsIgnoreCase("Change_Of_Credit_Category")){
			sheetNo=8;
		}else if(sheetName.equalsIgnoreCase("Change_Of_Cx_Segment")){
			sheetNo=9;
		}else if(sheetName.equalsIgnoreCase("Change_Of_Language")){
			sheetNo=10;
		}else if(sheetName.equalsIgnoreCase("Change_Of_DOB_DOR")){
			sheetNo=11;
		}else if(sheetName.equalsIgnoreCase("Change_Of_Bill_Dispatch_Type")){
			sheetNo=12;
		}else if(sheetName.equalsIgnoreCase("Change_Of_Email")){
			sheetNo=13;
		}else if(sheetName.equalsIgnoreCase("Change_Of_Ebill_Email")){
			sheetNo=14;
		}else if(sheetName.equalsIgnoreCase("Change_Of_ContactPerson")){
			sheetNo=15;
		}else if(sheetName.equalsIgnoreCase("Change_Of_Main_Package")){
			sheetNo=16;
		}else if(sheetName.equalsIgnoreCase("Change_Of_Data_Package")){
			sheetNo=17;
		}else if(sheetName.equalsIgnoreCase("Change_Of_AccountManagerDetails")){
			sheetNo=18;
		}else if(sheetName.equalsIgnoreCase("M2M_Migration")){
			sheetNo=19;
		}
					
		switch (sheetNo) {	
		case 1:   //Change_Of_Profile_Name
			mobileNo = data.get("MobileNo");
			String newTitle = data.get("NewTitle");
			String newSurname = data.get("NewSurname");
			String newOthernames = data.get("NewOtherNames");
			String oldTitle = data.get("OldTitle");
			String oldSurname = data.get("OldSurname");
			String oldOthernames = data.get("OldOtherNames");
			
			errorMessage = Change_Of_Name_In_SubscriberProfile(mobileNo, newTitle, newSurname, newOthernames, oldTitle, oldSurname, oldOthernames);
			writeToExcel(filePath, sheetName, mobileNo, errorMessage);
			break;
			
		case 2://"Change_Of_BillingAddress_Name"
			mobileNo = data.get("MobileNo");
			String oldNameToAppearOnBill = data.get("OldNameToAppearOnBill");
			String newNameToAppearOnBill = data.get("NewNameToAppearOnBill");

			errorMessage = Change_Of_Name_In_AddressChange(mobileNo, newNameToAppearOnBill, oldNameToAppearOnBill);
			writeToExcel(filePath, sheetName, mobileNo, errorMessage);
			break;

			
		case 3://"Change_Of_PR_Name":
			accountId = data.get("PrAccountId");
			String oldPrName = data.get("OldPrName");
			String newPrName = data.get("NewPrName");
			
			errorMessage = Change_Of_Name_In_AccountUpdate(accountId, newPrName, oldPrName);
			writeToExcel(filePath, sheetName, accountId, errorMessage);
			break;
				
		case 4://"Change_Of_ID":
			mobileNo = data.get("MobileNo");
			String oldIdentificationType = data.get("OldIdentificationType");
			String newIdentificationType = data.get("NewIdentificationType");
			String newIdentificationNumber = data.get("NewIdentificationNumber");
			String oldIdentificationNumber = data.get("OldIdentificationNumber");
			
			errorMessage = Change_Of_Id(mobileNo, newIdentificationType, newIdentificationNumber, oldIdentificationType, oldIdentificationNumber);
			writeToExcel(filePath, sheetName, mobileNo, errorMessage);
			break;

		case 5://"Change_Of_Credit_Limit":
			mobileNo = data.get("MobileNo");
			String newCreditLimit = data.get("NewCreditLimit");
			String oldCreditLimit = data.get("OldCreditLimit");
			
			errorMessage = Change_Of_Credit_Limit(mobileNo,newCreditLimit, oldCreditLimit);
			writeToExcel(filePath, sheetName, mobileNo, errorMessage);
			break;
			
		case 6://"Change_Of_Billing_Address":
			mobileNo = data.get("MobileNo");
			accountId = data.get("PrAccountId");
			String oldBillingAddressLine1 = data.get("OldBillingAddressLine1");
			String oldBillingAddressLine2 = data.get("OldBillingAddressLine2");
			String oldBillingAddressLine3 = data.get("OldBillingAddressLine3");
			String oldBillingPostalCode = data.get("OldBillingPostalCode");
			String newBillingAddressLine1 = data.get("NewBillingAddressLine1");
			String newBillingAddressLine2 = data.get("NewBillingAddressLine2");
			String newBillingAddressLine3 = data.get("NewBillingAddressLine3");
			String newBillingPostalCode = data.get("NewBillingPostalCode");
			String newAddressType = data.get("newAddressType");

			errorMessage = Change_Of_Billing_Address(mobileNo, accountId, newBillingAddressLine1, newBillingAddressLine2, newBillingAddressLine3, newBillingPostalCode, oldBillingAddressLine1, oldBillingAddressLine2, oldBillingAddressLine3, oldBillingPostalCode, newAddressType);
			writeToExcel(filePath, sheetName, mobileNo, errorMessage);
			break;
			
		case 7://"Change_Of_Credit_Type":
			mobileNo = data.get("MobileNo");
			String oldCreditType = data.get("OldCreditType");
			String newCreditType = data.get("NewCreditType");

			errorMessage = Change_Of_Credit_type(mobileNo, newCreditType, oldCreditType);
			writeToExcel(filePath, sheetName, mobileNo, errorMessage);
			break;

		case 8://"Change_Of_Credit_Category":
			mobileNo = data.get("MobileNo");
			String oldCustomerCategory = data.get("OldCustomerCategory");
			String newCustomerCategory = data.get("NewCustomerCategory");

			errorMessage = Change_Of_Credit_Category(mobileNo, newCustomerCategory, oldCustomerCategory);
			writeToExcel(filePath, sheetName, mobileNo, errorMessage);
			break;
			
		case 9:// "Change_Of_Cx_Segment":
			mobileNo = data.get("MobileNo");
			String oldSegmentationCode = data.get("OldSegmentationCode");
			String newSegmentationCode = data.get("NewSegmentationCode");

			errorMessage = Change_Segment_Code(mobileNo, newSegmentationCode, oldSegmentationCode);
			writeToExcel(filePath, sheetName, mobileNo, errorMessage);
			break;
			
		case 10://"Change_Of_Language":
			mobileNo = data.get("MobileNo");
			String oldCommunicationLanguage = data.get("OldCommunicationLanguage");
			String newCommunicationLanguage = data.get("NewCommunicationLanguage");
			
			errorMessage = change_of_Language(mobileNo, oldCommunicationLanguage, newCommunicationLanguage);
			writeToExcel(filePath, sheetName, mobileNo, errorMessage);
			break;
			
		case 11://"Change_Of_DOB_DOR":
			mobileNo = data.get("MobileNo");
			String oldDobDor = data.get("OldDOB_DOR");
			String newDobDor = data.get("NewDOB_DOR");
			
			errorMessage = Change_Of_DOB_OR_DOR(mobileNo, newDobDor, oldDobDor);
			writeToExcel(filePath, sheetName, mobileNo, errorMessage);
			break;
			
		case 12:// "Change_Of_Bill_Dispatch_Type":
			accountId = data.get("PrAccountId");
			String oldBillDispatchType = data.get("OldBillDispatchType");
			String newBillDispatchType = data.get("NewBillDispatchType");
			
			errorMessage = Change_Of_Bill_Dispatch_Type(accountId, newBillDispatchType, oldBillDispatchType);
			writeToExcel(filePath, sheetName, accountId, errorMessage);
			break;
			
		case 13://"Change_Of_Email":
			mobileNo = data.get("MobileNo");
			String addressId = data.get("AddressId");
			String oldBillingEmailAddress = data.get("OldBillingEmailAddress");
			String newBillingEmailAddress = data.get("NewBillingEmailAddress");
			
			errorMessage = Change_Of_Email_In_AddressChange(mobileNo, addressId, newBillingEmailAddress, oldBillingEmailAddress);
			writeToExcel(filePath, sheetName, mobileNo, errorMessage);
			break;
			
		case 14://"Change_Of_Ebill_Email": 
			mobileNo = data.get("MobileNo");
			String oldEBillEmailAddress = data.get("OldEBillEmailAddress");
			String newEBillEmailAddress = data.get("NewEBillEmailAddress");
			String newBillType = data.get("NewBillType");
			
			errorMessage = Change_Of_Email_In_EbillConfirmation(mobileNo, newEBillEmailAddress, oldEBillEmailAddress, newBillType);
			writeToExcel(filePath, sheetName, mobileNo, errorMessage);
			break;
			
		case 15://"Change_Of_ContactPerson": 
			mobileNo = data.get("MobileNo");
			String oldContactPerson = data.get("OldContactPerson");
			String oldContactNumber = data.get("OldContactNumber");
			String newContactPerson = data.get("NewContactPerson");
			String newContactNumber = data.get("NewContactNumber");
			
			errorMessage = Change_Of_ContactPerson(mobileNo, newContactPerson, newContactNumber, oldContactPerson, oldContactNumber);
			writeToExcel(filePath, sheetName, mobileNo, errorMessage);
			break;
			
		case 16://"Change_Of_Main_Package":
			mobileNo = data.get("MobileNo");
			String oldMainPackage = data.get("OldMainPackage");
			String newMainPackage = data.get("NewMainPackage");

			errorMessage = Change_Of_Main_Package(mobileNo, newMainPackage, oldMainPackage);
			writeToExcel(filePath, sheetName, mobileNo, errorMessage);
			break;
			
		case 17://"Change_Of_Data_Package":
			mobileNo = data.get("MobileNo");
			String oldDataPackage = data.get("OldDataPackage");
			String newDataPackage = data.get("NewDataPackage");
			String agentId = data.get("AgentId");		

			errorMessage = Change_Of_Data_Package(mobileNo, newDataPackage, oldDataPackage, agentId);
			writeToExcel(filePath, sheetName, mobileNo, errorMessage);
			break;
			
		case 18://"Change_Of_AccountManagerDetails":
			agentCode = data.get("OldDataCode");
			String oldAgentName = data.get("OldName");
			String oldAgentEmail = data.get("OldEmail");
			String newAgentCode = data.get("NewDataCode");
			String newAgentName = data.get("NewName");
			String newAgentEmail = data.get("NewEmail");

			errorMessage = Change_Of_Account_Manager_Details(agentCode, oldAgentName, oldAgentEmail,newAgentCode,newAgentName,newAgentEmail);
			writeToExcel(filePath, sheetName, agentCode, errorMessage);
			break;
			
		case 19: //M2M migration
			mobileNo =data.get("MobileNo");
			String idType=data.get("IDType");
			String idNo=data.get("IDNumber");
			String companyName=data.get("CompanyName");
			String billRunCode=data.get("BillRunCode");
			String addressL1=data.get("AddressLine1");
			String addressL2=data.get("AddressLine2");
			String addressL3=data.get("AddressLine3");
			String postalCode=data.get("PostalCode");
			String agentCode=data.get("AgentCode");
			String branchCode=data.get("BranchCode");
			String simNo=data.get("SIMNo");
			String imsiNo=data.get("IMSI");
			String creditLimit=data.get("CreditLimit");
			String creditType=data.get("CreditType");
			String customerCategory=data.get("CustomerCategory");
			String billFormat=data.get("BillFormat");
			String billDispatchType=data.get("BillDispatchType");
			String parentNode=data.get("ParentNode");

			errorMessage = M2M_Migration(mobileNo,idType,idNo,companyName,billRunCode,addressL1,addressL2,addressL3,postalCode,agentCode,branchCode,simNo,imsiNo,creditLimit,creditType,customerCategory,billFormat,billDispatchType,parentNode);
			writeToExcel(filePath, sheetName, mobileNo, errorMessage);
			break ;

		default:
			break;
		}

		}catch(Exception e){
			String outputList[]=e.getMessage().split("\\(");
			
			if(!(mobileNo==null)){
				writeToExcel(filePath, sheetName, mobileNo,outputList[0]);
			}
			else if(!(agentCode==null)){
				writeToExcel(filePath, sheetName, agentCode,outputList[0]);
			}
			else if(!(invoiceNo==null)){
				writeToExcel(filePath, sheetName, invoiceNo,outputList[0]);
			}
			else{
				writeToExcel(filePath, sheetName, accountId,outputList[0]);
			}
			driver.close();
			driver = null;
			Assert.fail(e.getMessage());
		}
		}
	}
	
	@AfterClass
	//TODO: update the email sender
	public void closeBrowser() throws MessagingException, IOException{
	//public void closeBrowser() throws IOException{
		EmailSender sendEmail = new EmailSender();
		sendEmail.sendEmail();
		copyResultSheetToResultFolder();
		driver.close();
		driver.quit();
	}
	
	//Scenarios Flows
	
	/**Method: M2M_Migration*/
	public String M2M_Migration(String mobileNo,String idType, String idNo, String companyName, String billRunCode, String addressL1, String addressL2, String addressL3, String postalCode, String agentCode, String branchCode, String simNo, String imsiNo, String creditLimit, String creditType, String customerCategory, String billFormat, String billDispatchType, String parentNode) throws Exception{
		
		String errorMessage="";

		cxRegistrationsHomePage=new CxRegistrationHomePage(driver);
		cxRegistrationsHomePage.fillMobileNo(mobileNo);
		cxRegistrationsHomePage.clickSearch();
		
		//Verify Id Number and Id Type
		String uiExistingIdType = cxRegistrationsHomePage.getIdType();
		if (!uiExistingIdType.equalsIgnoreCase(idType)) {
			errorMessage = errorMessage+"Expected Existing Id Type: '"+idType+"' is not match with the UI value: '"+uiExistingIdType+"'.";	
		}
		
		String uiExistingIdNumber = cxRegistrationsHomePage.getIdNumber();
		if (!uiExistingIdNumber.equalsIgnoreCase(idNo)) {
			errorMessage = errorMessage+"Expected Existing Id Number: '"+idNo+"' is not match with the UI value: '"+uiExistingIdNumber+"'.";
		}
			
		if(!errorMessage.equalsIgnoreCase("")){
			return errorMessage;
		}
		
		//Select a Profile	
		if(cxRegistrationsHomePage.isExistingProfile()){
			cxRegistrationsHomePage.selectProfile(companyName);		
			cxRegistrationsHomePage.clickEditAddress();
		}

		String uiExistingAddressL1 = cxRegistrationsHomePage.getAddressLine1();
		if(!uiExistingAddressL1.equalsIgnoreCase(addressL1)){
			cxRegistrationsHomePage.fillAddressLine1(addressL1);
		}
		
		String uiExistingAddressL2 = cxRegistrationsHomePage.getAddressLine2();
		if(!uiExistingAddressL2.equalsIgnoreCase(addressL2)){
			cxRegistrationsHomePage.fillAddressLine2(addressL2);
		}
		
		String uiExistingAddressL3 = cxRegistrationsHomePage.getAddressLine3();
		if(!uiExistingAddressL3.equalsIgnoreCase(addressL3)){
			cxRegistrationsHomePage.fillAddressLine3(addressL3);
		}
		
		String uiExistingPostalCode = cxRegistrationsHomePage.getPostalCode();
		if(!uiExistingPostalCode.equalsIgnoreCase(postalCode)){
			cxRegistrationsHomePage.selectNewPostalCode(postalCode);
		}
		
		String uiExistingBillRunCode = cxRegistrationsHomePage.getBillRunCode();
		if(!uiExistingBillRunCode.equalsIgnoreCase(billRunCode)){
			cxRegistrationsHomePage.selectBillRunCode(billRunCode);
		}
		
		//Fill Agent Details
		cxRegistrationsHomePage.enterAgentCode(agentCode);
		cxRegistrationsHomePage.enterBranchCode(branchCode);

		//Fill Connection Details
		String[] lstMobileNo = mobileNo.split(",");
		String[] lstSimNo = simNo.split(",");
		String[] lstImsiNo = imsiNo.split(",");
		
		for(int i=0; i<lstMobileNo.length; i++){
			cxRegistrationsHomePage.enterConnectionDetails(lstMobileNo[i].trim(), lstSimNo[i].trim());
			
			if(!cxRegistrationsHomePage.getImsi(lstMobileNo[i].trim()).equalsIgnoreCase(lstImsiNo[i].trim())){
				errorMessage = errorMessage+"Expected IMSI No: '"+lstImsiNo[i]+"' of SIM: "+lstSimNo[i]+" is not match with the UI value: '"+cxRegistrationsHomePage.getImsi(lstMobileNo[i])+"'.";
			}
			
			cxRegistrationsHomePage.fillCreditDetails(creditLimit, creditType, customerCategory);
			cxRegistrationsHomePage.fillBillingInfor(billFormat, billDispatchType);
			cxRegistrationsHomePage.searchHeirarchyAccount(parentNode);
			
			if(!cxRegistrationsHomePage.isHieararchyAccountExist()){
				errorMessage = errorMessage+"Expected Hieararchy Account: '"+parentNode+"' is not match with the UI value: '"+" "+"'.";
			}
			
			cxRegistrationsHomePage.selectHeirarchyAccount();
		}
		
		if(!errorMessage.equalsIgnoreCase("")){
			return errorMessage;
		}
		
		cxRegistrationsHomePage.clickSave();
		
		String message=cxRegistrationsHomePage.getRegistrationSuccessMessage();
		String messageContent=cxRegistrationsHomePage.getMsgContent();
		
		if (!message.equalsIgnoreCase("Success '"+lstMobileNo.length+"' out of '"+lstMobileNo.length+"' registrations.")) {
			errorMessage = "Expected Success Message: "+"Success '"+lstMobileNo.length+"' out of '"+lstMobileNo.length+"' registrations."+" is not macth with the UI value: '"+message+'\n'+messageContent+"'";
		}
		cxRegistrationsHomePage.clickReset();
		return errorMessage;	
	}

	/**Method: Change_Of_Account_Manager_Details*/
	public String Change_Of_Account_Manager_Details(String oldAgentCode, String oldAgentName, String oldAgentEmail, String newAgentCode, String newAgentName, String newAgentEmail) throws Exception{
	
		String errorMessage = "";		
		agentMasterPage = new AgentBranchMaintenencePage(driver);
		agentMasterPage.clickFindBtn();
		agentMasterPage.setAgentCode(oldAgentCode);
		agentMasterPage.clickExecuteBtn();
		
		String verifyOldValues= propFile.getPropertyValue("verifyOldVlaue");
		
		//Validate Existing Data
		if(verifyOldValues.equals("Y")){
			
			if(!oldAgentCode.equalsIgnoreCase("")){
				String uiExistingAgentCode = agentMasterPage.getAgentCode();
				if (!uiExistingAgentCode.equalsIgnoreCase(oldAgentCode)) {
					errorMessage = errorMessage+"Expected Existing Agent Code: '"+oldAgentCode+"' is not match with the UI value: '"+uiExistingAgentCode+"'.";	
				}
			}
			if(!oldAgentName.equalsIgnoreCase("")){
				String uiExistingAgentName = agentMasterPage.getAgentName();
				if (!uiExistingAgentName.equalsIgnoreCase(oldAgentName)) {
					errorMessage = errorMessage+"Expected Existing Agent Name: '"+oldAgentName+"' is not match with the UI value: '"+uiExistingAgentName+"'.";	
				}
			}
			if(!oldAgentEmail.equalsIgnoreCase("")){
				String uiExistingAgentEmail = agentMasterPage.getAgentEmail();
				if (!uiExistingAgentEmail.equalsIgnoreCase(oldAgentEmail)) {
					errorMessage = errorMessage+"Expected Existing Agent Email: '"+oldAgentEmail+"' is not match with the UI value: '"+uiExistingAgentEmail+"'.";	
				}
			}		
			if(!errorMessage.equalsIgnoreCase("")){
				return errorMessage;
			}
		}
		
		//Fill New Data
		if(!newAgentName.equalsIgnoreCase("")){
			agentMasterPage.setAgentName(newAgentName);
		}
		if(!newAgentEmail.equalsIgnoreCase("")){
			agentMasterPage.setAgentEmail(newAgentEmail);
		}
		agentMasterPage.clickSaveChanges();
		
		if(agentMasterPage.isPopupDisplayed()){
			errorMessage = "Changes Save Failed with error: "+agentMasterPage.getPopupMessage();
			return errorMessage;
		}		
		return errorMessage;			
	}

	
	/**Method: Change_Of_Name_In_SubscriberProfile*/
	public String Change_Of_Name_In_SubscriberProfile(String mobileNo, String newTitle, String newSurname, String newFirstName, String oldTitle, String oldSurname, String oldOtherName) throws Exception{
			
		String errorMessage = "";
		
		//Search By Mobile No
		ccbsHomePage = new CCBSHomePage(driver);
		ccbsHomePage.CCBSSearch("mobileNo",mobileNo);	
	
		//Go to Subscriber Profile Page
		ccbsHomePage.selectFromContNoRightClick("Modify");		
		subscriberProfilePage = new CCBSSubscriberProfilesUpdatePage(driver);
		
		String verifyOldValues= propFile.getPropertyValue("verifyOldVlaue");
	
		//Validate Existing Data
		if(verifyOldValues.equals("Y")){
			String uiExistingTitle = subscriberProfilePage.getTitle();
			if (!uiExistingTitle.equalsIgnoreCase(oldTitle.trim())) {
				errorMessage = errorMessage+"Expected Existing Title: '"+oldTitle+"' is not match with the UI value: '"+uiExistingTitle+"'.";			
			}
			
			String uiExistingSurname = subscriberProfilePage.getSurname();
			if (!oldSurname.equalsIgnoreCase(uiExistingSurname.trim())) {
				errorMessage = errorMessage+"Expected Existing Surname: '"+oldSurname+"' is not match with the UI value: '"+uiExistingSurname+"'.";
			}
	
			String uiExistingOthername = subscriberProfilePage.getOtherName();
			if (!uiExistingOthername.equalsIgnoreCase(oldOtherName.trim())) {
				errorMessage = errorMessage+"Expected Existing Other name: '"+uiExistingOthername+"' is not match with the UI value: '"+oldOtherName+"'.";		
			}
			if(!errorMessage.equalsIgnoreCase("")){
				subscriberProfilePage.clickBackToHotline();
				return errorMessage;
			}			
		}

		//Fill New Data
		if(!newTitle.equalsIgnoreCase("")){
			subscriberProfilePage.selectTitle(newTitle);
		}
		if(!newSurname.equalsIgnoreCase("")){
			subscriberProfilePage.fillSurname(newSurname);
		}
		if(!newFirstName.equalsIgnoreCase("")){
			subscriberProfilePage.fillOtherName(newFirstName);
		}		
		subscriberProfilePage.clickUpdate();	
	
		String popUpMsg = subscriberProfilePage.getPopUpMsg();	
		if (!popUpMsg.equalsIgnoreCase(StringsSubscriberProfileUpdatePage.UPDATE_SUCCESS_POPUP_MSG.value())) {
			errorMessage = "Expected Success message: '"+StringsSubscriberProfileUpdatePage.UPDATE_SUCCESS_POPUP_MSG.value()+"' is not match with the actual message: '"+popUpMsg+"'.";
			subscriberProfilePage.clickBackToHotline();
			return errorMessage;
		}
		
		subscriberProfilePage.clickBackToHotline();
		return errorMessage;
	}
	
	/**Method: Change_Of_Name_In_AddressChange
	 * @return */
	public String Change_Of_Name_In_AddressChange(String mobileNo, String newNameToAppearOnBill, String oldNameToAppearOnBill) throws Exception{

		String errorMessage = "";
		
		//Search By Mobile No
		ccbsHomePage = new CCBSHomePage(driver);
		ccbsHomePage.CCBSSearch("mobileNo", mobileNo);
		
		//Go to Address Change Page
		ccbsHomePage.selectFromContNoRightClick("Address");
		addressPage = new CCBSAddressPage(driver);
		
		String verifyOldValues= propFile.getPropertyValue("verifyOldVlaue");
		
		//Validate Existing Data
		if(verifyOldValues.equalsIgnoreCase("Y")){
			String uiExistingNameToAppearOnBill = addressPage.getNameToApperaOnBill();
			if(!uiExistingNameToAppearOnBill.equalsIgnoreCase(oldNameToAppearOnBill)){
				errorMessage = "Expected Existing Name To Appear On Bill: '"+oldNameToAppearOnBill+"' is not match with the UI Value: '"+uiExistingNameToAppearOnBill+"'.";
				addressPage.clickBackToHotlineAddressChangePage();
				return errorMessage;
			}
		}
		
		//Fill New Data
		addressPage.fillNameToApperaOnBill(newNameToAppearOnBill);
		addressPage.clickSaveAdressChange();
		
		String popUpMsg = addressPage.getPopupMessage();	
		if(!popUpMsg.equalsIgnoreCase(StringsAddressChangePage.UPDATE_ADDRESS_SUCCESS_MSG.value())){
			errorMessage = "Expected Success message: '"+StringsAddressChangePage.UPDATE_ADDRESS_SUCCESS_MSG.value()+"' is not match with the actual message: '"+popUpMsg+"'.";
			addressPage.clickBackToHotlineAddressChangePage();
			return errorMessage;
		}
		
		addressPage.clickBackToHotlineAddressChangePage();
		return errorMessage;
	}

	/**Method: Change_Of_Name_In_AccountUpdate
	 * @return */
	public String Change_Of_Name_In_AccountUpdate(String accountId, String newDescription, String oldDescription) throws Exception{

		String errorMessage = "";
		
		//Search By Account Id
		ccbsHomePage = new CCBSHomePage(driver);
		ccbsHomePage.CCBSSearch("accountNo",accountId);

		//Go to Account Updates Page
		ccbsHomePage.selectFromAcIdRightClick("Account Updates");
		accountUpdatePage = new CCBSAccountUpdatePage(driver);
		
		String verifyOldValues= propFile.getPropertyValue("verifyOldVlaue");
		
		//Validate Existing Data
		if(verifyOldValues.equalsIgnoreCase("Y")){
			String uiExistingDescription = accountUpdatePage.getDescription(accountId);
			if(!uiExistingDescription.equalsIgnoreCase(oldDescription)){
				errorMessage = "Expected Existing Description: '"+oldDescription+"' is not match with the UI value: '"+uiExistingDescription+"'.";
				accountUpdatePage.clickBackToHotline();
				return errorMessage;
			}
		}
		//Fill New Data
		accountUpdatePage.fillDescription(accountId, newDescription);
		accountUpdatePage.clickSave();
		
		String popUpMsg = accountUpdatePage.getPopupMessage();
		if(!popUpMsg.equalsIgnoreCase(StringsAccountUpdatePage.ACCOUNT_UPDATE_SUCCESS_MSG.value())){
			errorMessage = "Expected Success message: '"+StringsAccountUpdatePage.ACCOUNT_UPDATE_SUCCESS_MSG.value()+"' is not match with the actual message: '"+popUpMsg+"'.";
			accountUpdatePage.clickBackToHotline();
			return errorMessage;
		}
		
		accountUpdatePage.clickBackToHotline();
		return errorMessage;
	}
	
	/**Method: Change_Of_Id
	 * @return */
	public String Change_Of_Id(String mobileNo, String newIdType, String newIdNumber, String oldIdType, String oldIdNumber) throws Exception{
		
		String errorMessage = "";
		
		//Search By Mobile No
		ccbsHomePage=new CCBSHomePage(driver);
		ccbsHomePage.CCBSSearch("mobileNo",mobileNo);
		
		//Go to Subscriber Profile Page
		ccbsHomePage.selectFromContNoRightClick("Modify");	
		subscriberProfilePage =new CCBSSubscriberProfilesUpdatePage(driver);
		
		String verifyOldValues= propFile.getPropertyValue("verifyOldVlaue");
		
		//Validate Existing Data
		if(verifyOldValues.equalsIgnoreCase("Y")){
			String uiExistingIdType = subscriberProfilePage.getIdType();
			if(!uiExistingIdType.equalsIgnoreCase(oldIdType)){
				errorMessage = errorMessage+ "Expected Existing ID Type: '"+oldIdType+"' is not match with the UI value: '"+uiExistingIdType+"'.";
			}		
			String uiExistingIdNumber = subscriberProfilePage.getIdNo();
			if(!uiExistingIdNumber.equalsIgnoreCase(oldIdNumber)){
				errorMessage = errorMessage+ "Expected Existing ID Number: '"+oldIdNumber+"' is not match with the UI value: '"+uiExistingIdNumber+"'.";
			}		
			if(!errorMessage.equalsIgnoreCase("")){
				subscriberProfilePage.clickBackToHotline();
				return errorMessage;
			}	
		}
		
		//Fill New Data
		if(!newIdType.equalsIgnoreCase("")){
			subscriberProfilePage.selectIdType(newIdType);
		}
		if(!newIdNumber.equalsIgnoreCase("")){
			subscriberProfilePage.changeIdNo(newIdNumber);
		}
		
		subscriberProfilePage.clickUpdate();
		
		String popUpMsg=subscriberProfilePage.getPopUpMsg();	
		if(!popUpMsg.equalsIgnoreCase(StringsSubscriberProfileUpdatePage.UPDATE_SUCCESS_POPUP_MSG.value())){
			errorMessage = "Expected Success message: '"+StringsSubscriberProfileUpdatePage.UPDATE_SUCCESS_POPUP_MSG.value()+"' is not match with the actual message: '"+popUpMsg+"'.";
			subscriberProfilePage.clickBackToHotline();
			return errorMessage;
		}
		
		subscriberProfilePage.clickBackToHotline();
		return errorMessage;
	}
	
	/**Method: Change_Of_Credit_Limit
	 * @return */
	public String Change_Of_Credit_Limit(String mobileNo, String newCreditLimit, String oldCreditLimit) throws Exception{

		String errorMessage = "";
		
		//Search by Mobile No
		ccbsHomePage=new CCBSHomePage(driver);
		ccbsHomePage.CCBSSearch("mobileNo",mobileNo);
		
		//Go to Contract Update Page
		ccbsHomePage.clickUpdate();
		contractUpdatePage = new CCBSContractUpdatePage(driver);
		
		String verifyOldValues= propFile.getPropertyValue("verifyOldVlaue");
		
		//Validate Existing Data
		if(verifyOldValues.equalsIgnoreCase("Y")){
			String uiCreditLimit = contractUpdatePage.getCreditLimit();
			if(!uiCreditLimit.equalsIgnoreCase(oldCreditLimit)){
				errorMessage = "Expected Existing Credit Limit: '"+oldCreditLimit+"' is not match with the UI value: '"+uiCreditLimit+"'.";
				contractUpdatePage.clickBackToHotline();
				return errorMessage;
			}
		}
		
		//Fill New Data
		contractUpdatePage.fillCreditLimit(newCreditLimit);
		contractUpdatePage.clickSaveChanges();
		
		String popUpMsg = contractUpdatePage.getAttributeChangeMessage();
		if(!popUpMsg.equalsIgnoreCase(StringsContractUpdatePage.ATTRIBUTE_CHANGE_SUCCESS_MSG.value())){
			errorMessage = "Expected Success message: '"+StringsContractUpdatePage.ATTRIBUTE_CHANGE_SUCCESS_MSG.value()+"' is not match with the actual message: '"+popUpMsg+"'.";
			contractUpdatePage.clickBackToHotline();
			return errorMessage;
		}
	
		contractUpdatePage.clickBackToHotline();
		return errorMessage;
	}

	/**Method: Change_Of_Billing_Address
	 * @return */
	public String Change_Of_Billing_Address(String mobileNo, String accountId, String newAddressLine1, String newAddressLine2, String newAddressLine3, String newPostalCode, String oldAddressLine1, String oldAddressLine2, String oldAddressLine3, String oldPostalCode, String addressType) throws Exception{

		String errorMessage = "";
		
		//Search by Mobile No
		ccbsHomePage=new CCBSHomePage(driver);
		ccbsHomePage.CCBSSearch("mobileNo",mobileNo);
		
		//Go to Address Change Page
		ccbsHomePage.selectFromContNoRightClick("Address");
		addressPage = new CCBSAddressPage(driver);
		
		String verifyOldValues= propFile.getPropertyValue("verifyOldVlaue");
		
		//Validate Existing Data
		if(verifyOldValues.equalsIgnoreCase("Y")){
		String uiExistingAddressLine1 = addressPage.getAddressLine1();
			if(!uiExistingAddressLine1.equalsIgnoreCase(oldAddressLine1)){
				errorMessage = errorMessage+ "Expected Existing Address Line 1: '"+oldAddressLine1+"' is not match with the Ui Value: '"+uiExistingAddressLine1+"'.";
			}
			String uiExistingAddressLine2 = addressPage.getAddressLine2();
			if(!uiExistingAddressLine2.equalsIgnoreCase(oldAddressLine2)){
				errorMessage = errorMessage+ "Expected Existing Address Line 2: '"+oldAddressLine2+"' is not match with the Ui Value: '"+uiExistingAddressLine2+"'.";
			}
			String uiExistingAddressLine3 = addressPage.getAddressLine3();
			if(!uiExistingAddressLine3.equalsIgnoreCase(oldAddressLine3)){
				errorMessage = errorMessage+ "Expected Existing Address Line 3: '"+oldAddressLine3+" is not match with the Ui Value: '"+uiExistingAddressLine3+"'.";
			}
			String uiExistingPostalCode = addressPage.getPostalCode();
			if(!uiExistingPostalCode.equalsIgnoreCase(oldPostalCode)){
				errorMessage = errorMessage+ "Expected Existing Postal Code: '"+oldPostalCode+"' is not match with the Ui Value: '"+uiExistingPostalCode+"'.";
			}
			if(!errorMessage.equalsIgnoreCase("")){
				addressPage.clickBackToHotlineAddressChangePage();
				return errorMessage;
			}
		}

		//Fill New Data
		addressPage.clickCreateNew();
		if(!newAddressLine1.equalsIgnoreCase("")){
			addressPage.fillNewAddressLine1(newAddressLine1);
		}
		if(!newAddressLine2.equalsIgnoreCase("")){
			addressPage.fillNewAddressLine2(newAddressLine2);
		}
		if(!newAddressLine3.equalsIgnoreCase("")){
			addressPage.fillNewAddressLine3(newAddressLine3);
		}
		if(!newPostalCode.equalsIgnoreCase("")){
			addressPage.selectNewPostalCode(newPostalCode);
		}
		addressPage.selectAddressType(addressType);
		addressPage.clickSave();

		String addressChangePopUpMsg = addressPage.getInformationPopupMsg();
		if(!(addressChangePopUpMsg.substring(0, 15)).equalsIgnoreCase(StringsAddressChangePage.ADDRESS_CREATED_SUCCESS_MSG.value())){
			errorMessage = "Expected Success message: "+StringsAddressChangePage.ADDRESS_CREATED_SUCCESS_MSG.value()+" is not match with the actual message: '"+addressChangePopUpMsg+"'.";
			addressPage.clickBackToHotlineCreateAddressPage();	
			addressPage.clickBackToHotlineAddressChangePage();
			return errorMessage;
		}

		String newAddressId = addressPage.getNewAddressId(addressChangePopUpMsg);
		
		addressPage.clickBackToHotlineCreateAddressPage();	
		addressPage.clickBackToHotlineAddressChangePage();
		
		//Search by Account Id
		ccbsHomePage.CCBSSearch("accountNo",accountId);
		
		//Go to Account Updates Page
		ccbsHomePage.selectFromAcIdRightClick("Account Updates");
		accountUpdatePage = new CCBSAccountUpdatePage(driver);
		
		accountUpdatePage.fillAddressId(accountId, newAddressId);
		accountUpdatePage.clickSave();
		
		String popUpMsg = accountUpdatePage.getPopupMessage();
		if(!popUpMsg.equalsIgnoreCase(StringsAccountUpdatePage.ACCOUNT_UPDATE_SUCCESS_MSG.value())){
			errorMessage = "Expected Success message: '"+StringsAccountUpdatePage.ACCOUNT_UPDATE_SUCCESS_MSG.value()+"' is not match with the actual message: '"+popUpMsg+"'.";
			accountUpdatePage.clickBackToHotline();
			return errorMessage;
		}
		
		accountUpdatePage.clickBackToHotline();
		return errorMessage;
	}

	/**Method: Change_Of_Credit_type
	 * @return */
	public String Change_Of_Credit_type(String mobileNo, String newCreditType, String oldCreditType) throws Exception{
		
		String errorMessage = "";
				
		//Search by Mobile No
		ccbsHomePage=new CCBSHomePage(driver);
		ccbsHomePage.CCBSSearch("mobileNo",mobileNo);
		
		//Goto Contract Update Page
		ccbsHomePage.clickUpdate();
		contractUpdatePage = new CCBSContractUpdatePage(driver);
		
		String verifyOldValues= propFile.getPropertyValue("verifyOldVlaue");
		
		//Validate Existing Data
		if(verifyOldValues.equalsIgnoreCase("Y")){
			String uiCredittype = contractUpdatePage.getCreditType();
			if(!uiCredittype.equalsIgnoreCase(oldCreditType)){
				errorMessage = "Expected Existing Credit Type: '"+oldCreditType+"' is not match with the UI value: '"+uiCredittype+"'.";
				contractUpdatePage.clickBackToHotline();
				return errorMessage;
			}
		}
			
		//Fill new Data
		contractUpdatePage.selectCreditType(newCreditType);
		contractUpdatePage.clickSaveChanges();
		
		String attributeChangeMsg = contractUpdatePage.getAttributeChangeMessage();
		if(!attributeChangeMsg.equalsIgnoreCase(StringsContractUpdatePage.ATTRIBUTE_CHANGE_SUCCESS_MSG.value())){
			errorMessage = "Expected Success message: '"+StringsContractUpdatePage.ATTRIBUTE_CHANGE_SUCCESS_MSG.value()+"' is not match with the actual message: '"+attributeChangeMsg+"'.";
			contractUpdatePage.clickBackToHotline();
			return errorMessage;
		}
		
		contractUpdatePage.clickBackToHotline();
		return errorMessage;
	}
	
	/**Method: Change_Of_Credit_Category
	 * @return */
	public String Change_Of_Credit_Category(String mobileNo, String newCreditCategory, String oldCreditCategory) throws Exception{

		String errorMessage = "";
		
		//Search by Mobile No
		ccbsHomePage=new CCBSHomePage(driver);
		ccbsHomePage.CCBSSearch("mobileNo",mobileNo);
		
		//Go to Contract Update Page
		ccbsHomePage.clickUpdate();
		contractUpdatePage = new CCBSContractUpdatePage(driver);
		
		String verifyOldValues= propFile.getPropertyValue("verifyOldVlaue");
		
		//Validate Existing Data
		if(verifyOldValues.equalsIgnoreCase("Y")){
			String uiCreditCategory = contractUpdatePage.getCustomerCategory();
			if(!uiCreditCategory.equalsIgnoreCase(oldCreditCategory)){
				errorMessage = "Expected Existing Credit Category: '"+oldCreditCategory+"' is not match with the UI value: '"+uiCreditCategory+"'.";
				contractUpdatePage.clickBackToHotline();
				return errorMessage;
			}
		}
				
		//Fill New Data
		contractUpdatePage.selectCustomerCategory(newCreditCategory);
		contractUpdatePage.clickSaveChanges();
		
		String attributeChangeMsg = contractUpdatePage.getAttributeChangeMessage();
		if(!attributeChangeMsg.equalsIgnoreCase(StringsContractUpdatePage.ATTRIBUTE_CHANGE_SUCCESS_MSG.value())){
			errorMessage = "Expected Success message: '"+StringsContractUpdatePage.ATTRIBUTE_CHANGE_SUCCESS_MSG.value()+"' is not match with the actual message: '"+attributeChangeMsg+"'.";
			contractUpdatePage.clickBackToHotline();
			return errorMessage;
		}
		
		contractUpdatePage.clickBackToHotline();
		return errorMessage;
	}
	
	/**Method: Change_Segment_Code
	 * @return */
	public String Change_Segment_Code(String mobileNo, String newSegmentCode, String oldSegmentCode) throws Exception{

		String errorMessage = "";
		
		//Search by Mobile No
		ccbsHomePage=new CCBSHomePage(driver);
		ccbsHomePage.CCBSSearch("mobileNo",mobileNo);
		
		//Go to Subscriber Profile Page
		ccbsHomePage.selectFromContNoRightClick("Modify");	
		subscriberProfilePage =new CCBSSubscriberProfilesUpdatePage(driver);	
		
		String verifyOldValues= propFile.getPropertyValue("verifyOldVlaue");
		
		//Validate Existing Data
		if(verifyOldValues.equalsIgnoreCase("Y")){
			String uiSegmentCode = subscriberProfilePage.getSegmentCode();
			if(!uiSegmentCode.equalsIgnoreCase(oldSegmentCode)){
				errorMessage = "Expected Existing Segment Code: '"+oldSegmentCode+"' is not match with the UI value: '"+uiSegmentCode+"'.";
				subscriberProfilePage.clickBackToHotline();
				return errorMessage;
			}
		}
		
		//Fill New Data
		if(!newSegmentCode.equalsIgnoreCase("")){
			subscriberProfilePage.changeSegmentCode(newSegmentCode);
		}		
		subscriberProfilePage.clickUpdate();
		
		String popUpMsg=subscriberProfilePage.getPopUpMsg();
		if(!popUpMsg.equalsIgnoreCase(StringsSubscriberProfileUpdatePage.UPDATE_SUCCESS_POPUP_MSG.value())){
			errorMessage = "Expected Success message: '"+StringsSubscriberProfileUpdatePage.UPDATE_SUCCESS_POPUP_MSG.value()+"' is not match with the actual message: '"+popUpMsg+"'.";
			subscriberProfilePage.clickBackToHotline();
			return errorMessage;		
		}		
		subscriberProfilePage.clickBackToHotline();
		return errorMessage;
	}
	
	/**Method: change_of_Language
	 * @return */
	public String change_of_Language(String mobileNo, String oldLanguage, String newLanguage)  throws Exception{

		String errorMessage = "";
		
		//Search By Mobile No
		ccbsHomePage=new CCBSHomePage(driver);
		ccbsHomePage.CCBSSearch("mobileNo",mobileNo);
		
		//Go to Subscriber Profile Page
		ccbsHomePage.selectFromContNoRightClick("Modify");	
		subscriberProfilePage =new CCBSSubscriberProfilesUpdatePage(driver);	
		
		String verifyOldValues= propFile.getPropertyValue("verifyOldVlaue");
		
		//Validate Existing Data
		if(verifyOldValues.equalsIgnoreCase("Y")){
			String uiExistingLanguage = subscriberProfilePage.getLanguage();
			if(!uiExistingLanguage.equalsIgnoreCase(oldLanguage)){
				errorMessage = "Expected Existing Language: '"+oldLanguage+"' is not match with the UI value: '"+uiExistingLanguage+"'.";
				subscriberProfilePage.clickBackToHotline();
				return errorMessage;
			}
		}
		
		//Fill New Data
		subscriberProfilePage.selectLanguage(newLanguage);
		subscriberProfilePage.clickUpdate();
		
		String popUpMsg=subscriberProfilePage.getPopUpMsg();
		if(!popUpMsg.equalsIgnoreCase(StringsSubscriberProfileUpdatePage.UPDATE_SUCCESS_POPUP_MSG.value())){
			errorMessage = "Expected Success message: '"+StringsSubscriberProfileUpdatePage.UPDATE_SUCCESS_POPUP_MSG.value()+"' is not match with the actual message: '"+popUpMsg+"'.";
			subscriberProfilePage.clickBackToHotline();
			return errorMessage;
		}
		
		subscriberProfilePage.clickBackToHotline();
		return errorMessage;
	}
	
	/**Method: Change_Of_DOB_OR_DOR
	 * @return */
	public String Change_Of_DOB_OR_DOR(String mobileNo, String newDobDor, String oldDobDor) throws Exception{

		String errorMessage = "";
		
		//Search By Mobile No
		ccbsHomePage=new CCBSHomePage(driver);
		ccbsHomePage.CCBSSearch("mobileNo",mobileNo);
		
		//Go to Subscriber Profile Page
		ccbsHomePage.selectFromContNoRightClick("Modify");
		subscriberProfilePage =new CCBSSubscriberProfilesUpdatePage(driver);	
		
		String verifyOldValues= propFile.getPropertyValue("verifyOldVlaue");
		
		//Validate Existing Data
		if(verifyOldValues.equalsIgnoreCase("Y")){
			String uiExistingDobDor = subscriberProfilePage.getDOBOrDOR();
			if(!uiExistingDobDor.equalsIgnoreCase(oldDobDor)){
				errorMessage = "Expected Existing Dob or Dor: '"+oldDobDor+"' is not match with the UI value: '"+uiExistingDobDor+"'.";
				subscriberProfilePage.clickBackToHotline();
				return errorMessage;
			}
		}

		//Fill New Data
		subscriberProfilePage.changeDOBOrDOR(newDobDor);
		subscriberProfilePage.clickUpdate();
		
		String popUpMsg=subscriberProfilePage.getPopUpMsg();
		if(!popUpMsg.equalsIgnoreCase(StringsSubscriberProfileUpdatePage.UPDATE_SUCCESS_POPUP_MSG.value())){
			errorMessage = "Expected Success message: '"+StringsSubscriberProfileUpdatePage.UPDATE_SUCCESS_POPUP_MSG.value()+"' is not match with the actual message: '"+popUpMsg+"'.";
			subscriberProfilePage.clickBackToHotline();
			return errorMessage;
			
		}
		
		subscriberProfilePage.clickBackToHotline();
		return errorMessage;
	}
	
	/**Method: Change_Of_Bill_Dispatch_Type
	 * @return */
	public String Change_Of_Bill_Dispatch_Type(String accountId, String newBillDispatchType, String oldBillDispatchType) throws Exception{
		
		String errorMessage = "";
		
		//Search by Account Id
		ccbsHomePage=new CCBSHomePage(driver);
		ccbsHomePage.CCBSSearch("accountNo",accountId);
		
		//Go to Account Update Page
		ccbsHomePage.selectFromAcIdRightClick("Account Updates");
		accountUpdatePage = new CCBSAccountUpdatePage(driver);
		
		String verifyOldValues= propFile.getPropertyValue("verifyOldVlaue");
		
		//Validate Existing Data
		if(verifyOldValues.equalsIgnoreCase("Y")){
			String uiBillDispatchType = accountUpdatePage.getBillDispatchType(accountId);
			if(!uiBillDispatchType.equalsIgnoreCase(oldBillDispatchType)){
				errorMessage = "Expected Existing Bill Dispatch Type: '"+oldBillDispatchType+"' is not match with the UI value: '"+uiBillDispatchType+"'.";
				accountUpdatePage.clickBackToHotline();
				return errorMessage;
			}
		}

		//Fill New Data
		accountUpdatePage.selectBillDispatchType(accountId, newBillDispatchType);
		accountUpdatePage.clickSave();
		
		String popUpMsg = accountUpdatePage.getPopupMessage();
		if(!popUpMsg.equalsIgnoreCase(StringsAccountUpdatePage.ACCOUNT_UPDATE_SUCCESS_MSG.value())){
			errorMessage = "Expected Success message: '"+StringsAccountUpdatePage.ACCOUNT_UPDATE_SUCCESS_MSG.value()+"' is not match with the actual message: '"+popUpMsg+"'.";
			accountUpdatePage.clickBackToHotline();
			return errorMessage;
		}
		
		accountUpdatePage.clickBackToHotline();
		return errorMessage;
	}
	
	/**Method: Change_Of_Email_In_AddressChange
	 * @return */
	public String Change_Of_Email_In_AddressChange(String mobileNo, String addressId, String newEmail, String oldEmail) throws Exception{
		
		String errorMessage = "";

		//Search by Mobile No
		ccbsHomePage=new CCBSHomePage(driver);
		ccbsHomePage.CCBSSearch("mobileNo",mobileNo);
		
		//Go to Address Change Page
		ccbsHomePage.selectFromContNoRightClick("Address");
		addressPage = new CCBSAddressPage(driver);
		
		//Validate Existing Data
		String uiEmail = addressPage.getEmailAddress(addressId);
		if(!uiEmail.equalsIgnoreCase(oldEmail)){
			errorMessage = "Expected Existing Email: '"+oldEmail+"' is not match with the UI value: '"+uiEmail+"'.";
			addressPage.clickBackToHotlineAddressChangePage();
			return errorMessage;
		}
		
		//Fill New Data
		addressPage.fillEmailAddress(addressId, newEmail);
		addressPage.clickSaveAdressChange();
		
		String popUpMsg = addressPage.getPopupMessage();	
		if(!popUpMsg.equalsIgnoreCase(StringsAddressChangePage.UPDATE_ADDRESS_SUCCESS_MSG.value())){
			errorMessage = "Expected Success message: '"+StringsAddressChangePage.UPDATE_ADDRESS_SUCCESS_MSG.value()+"' is not match with the actual message: '"+popUpMsg+"'.";
			addressPage.clickBackToHotlineAddressChangePage();
			return errorMessage;
			
		}
		
		addressPage.clickBackToHotlineAddressChangePage();
		return errorMessage;
	}
	
	/**Method: Change_Of_Email_In_EbillConfirmation
	 * @return */
	public String Change_Of_Email_In_EbillConfirmation(String mobileNo, String newEmail, String oldEmail, String billType) throws Exception{

		String errorMessage = "";
		
		//Search by Mobile No
		ccbsHomePage=new CCBSHomePage(driver);
		ccbsHomePage.CCBSSearch("mobileNo",mobileNo);
		
		//Go to E-Bill Confirmation Page
		ccbsHomePage.selectFromCxRegRightClick("E-Bill Confirmation");
		ebillConfirmationPage = new CCBSEbillConfirmationPage(driver);
		
		String verifyOldValues= propFile.getPropertyValue("verifyOldVlaue");
		
		//Validate Existing Data
		if(verifyOldValues.equalsIgnoreCase("Y")){
			if(!oldEmail.equalsIgnoreCase("")){			
				String uiExistingEmail = ebillConfirmationPage.getExistingEmail();
				if(!uiExistingEmail.equalsIgnoreCase(oldEmail)){
					errorMessage = "Expected Existing Email: '"+oldEmail+"' is not match with the UI value: '"+uiExistingEmail+"'.";
					ebillConfirmationPage.clickBackToHotline();
					return errorMessage;
				}
				ebillConfirmationPage.selectOldBillRequest(oldEmail, "NO");
			}
		}

		//Fill New Data
		ebillConfirmationPage.fillEmailAddress(newEmail);
		ebillConfirmationPage.selectBillType(billType);
		ebillConfirmationPage.selectBillRequest("YES");
		ebillConfirmationPage.clickAdd();
		
		String updateSuccessMsg = ebillConfirmationPage.getPopupMessage();
		if(!updateSuccessMsg.equalsIgnoreCase(StringsEbillConfirmationPage.EMAIL_UPDATE_SUCCESS_MSG.value())){
			errorMessage = "Expected Success message: '"+StringsEbillConfirmationPage.EMAIL_UPDATE_SUCCESS_MSG.value()+"' is not match with the actual message: '"+updateSuccessMsg+"'.";
			ebillConfirmationPage.clickBackToHotline();
			return errorMessage;
			
		}
		ebillConfirmationPage.clickSaveAll();
		
		String addSuccessMsg = ebillConfirmationPage.getPopupMessage();
		if(!addSuccessMsg.equalsIgnoreCase(StringsEbillConfirmationPage.ADD_NEW_EMAIL_SUCCESS_MSG.value())){
			errorMessage = "Expected Success message: '"+StringsEbillConfirmationPage.ADD_NEW_EMAIL_SUCCESS_MSG.value()+"' is not match with the actual message: '"+addSuccessMsg+"'.";
			ebillConfirmationPage.clickBackToHotline();
			return errorMessage;
			
		}

		ebillConfirmationPage.clickBackToHotline();
		return errorMessage;
	}
	
	/**Method: Change_Of_ContactPerson
	 * @return */
	public String Change_Of_ContactPerson(String mobileNo, String newContactPerson, String newContactNo, String oldContactPerson, String oldContactNo) throws Exception{

		String errorMessage = "";
		
		//Search by Mobile No
		ccbsHomePage=new CCBSHomePage(driver);
		ccbsHomePage.CCBSSearch("mobileNo",mobileNo);
		
		//Go to Subscriber Profile Page
		ccbsHomePage.selectFromContNoRightClick("Modify");	
		subscriberProfilePage =new CCBSSubscriberProfilesUpdatePage(driver);
		
		String verifyOldValues= propFile.getPropertyValue("verifyOldVlaue");
		
		//Validate Existing Data
		if(verifyOldValues.equalsIgnoreCase("Y")){
			String uiContactPerson = subscriberProfilePage.getContactPerson();
			if(!uiContactPerson.equalsIgnoreCase(oldContactPerson)){
				errorMessage = errorMessage+ "Expected Existing Contact Person: '"+oldContactPerson+"' is not match with the UI value: '"+uiContactPerson+"'.";
			}
			String uiContactNo = subscriberProfilePage.getContactPersonNumber();
			if(!uiContactNo.equalsIgnoreCase(oldContactNo)){
				errorMessage = errorMessage+ "Expected Existing Contact No: '"+oldContactNo+"' is not match with the UI value: '"+uiContactNo+"'.";
			}
			if(!errorMessage.equalsIgnoreCase("")){
				subscriberProfilePage.clickBackToHotline();
				return errorMessage;
			}
		}
		
		//Fill New Data
		if(!newContactPerson.equalsIgnoreCase("")){
			subscriberProfilePage.changeContactPerson(newContactPerson);
		}
		if(!newContactNo.equalsIgnoreCase("")){
			subscriberProfilePage.changeContactNo(newContactNo);
		}
		subscriberProfilePage.clickUpdate();
		
		String popUpMsg=subscriberProfilePage.getPopUpMsg();
		if(!popUpMsg.equalsIgnoreCase(StringsSubscriberProfileUpdatePage.UPDATE_SUCCESS_POPUP_MSG.value())){
			errorMessage = "Expected Success message: '"+StringsSubscriberProfileUpdatePage.UPDATE_SUCCESS_POPUP_MSG.value()+"' is not match with the actual message: '"+popUpMsg+"'.";
			subscriberProfilePage.clickBackToHotline();
			return errorMessage;
			
		}
		
		subscriberProfilePage.clickBackToHotline();
		return errorMessage;
	}
	
	/**Method: Change_Of_Main_Package
	 * @return */
	public String Change_Of_Main_Package(String mobileNo, String newMainPackageCode, String oldMainPackageCode) throws Exception{
		
		String errorMessage = "";
		
		//Search By Mobile No
		ccbsHomePage=new CCBSHomePage(driver);
		ccbsHomePage.CCBSSearch("mobileNo",mobileNo);
		
		//Go to Package Change Page
		ccbsHomePage.clickPackageChange();
		packageChange = new CCBSPackageChangePage(driver);
		
		String verifyOldValues= propFile.getPropertyValue("verifyOldVlaue");
		
		//Validate Existing Data
		if(verifyOldValues.equalsIgnoreCase("Y")){
			String uiExistingPackage = packageChange.getExistingPackageCode();
			if(!uiExistingPackage.equalsIgnoreCase(oldMainPackageCode)){
				errorMessage = "Expected Existing Package Code: '"+oldMainPackageCode+"' is not match with the UI value: '"+uiExistingPackage+"'.";
				packageChange.clickBackToHotline();
				return errorMessage;
			}
		}
		
		//Fill New Data
		packageChange.clickSelectPackageCode();
		packageChange.selectNewPackage(newMainPackageCode);
		packageChange.clickOkNewPackageList();
		packageChange.clickPackageChange();
		
		String popupMsg = packageChange.getPackageChangeSuccessMsg();
		if(!popupMsg.equalsIgnoreCase(StringsPackageChangePage.GSM_PACKAGE_CHANGE_SUCCESS_MSG.value())){
			errorMessage = "Expected Success message: '"+StringsPackageChangePage.GSM_PACKAGE_CHANGE_SUCCESS_MSG.value()+"' is not match with the actual message: '"+popupMsg+"'.";
			packageChange.clickSuccessPopupOk();
			packageChange.clickBackToHotline();
			return errorMessage;
		}
		
		packageChange.clickSuccessPopupOk();
		packageChange.clickBackToHotline();
		return errorMessage;
	}
	
	/**Method: Change_Of_Data_Package
	 * @return */
	public String Change_Of_Data_Package(String mobileNo, String newDataPackageCode, String oldDataPackageId, String agentId) throws Exception{
	
		String errorMessage = "";
		
		//Search By Mobile No
		ccbsHomePage=new CCBSHomePage(driver);
		ccbsHomePage.CCBSSearch("mobileNo",mobileNo);
		
		//Go to Package Change Page
		ccbsHomePage.clickPackageChange();
		packageChange = new CCBSPackageChangePage(driver);
		
		//Select Existing Data Package
		packageChange.clickChangeSelection();
		packageChange.fillConnectedPackageCode(oldDataPackageId);	
		
		String verifyOldValues= propFile.getPropertyValue("verifyOldVlaue");
		
		//Validate Existing Data
		if(verifyOldValues.equalsIgnoreCase("Y")){
			if(!packageChange.isExistingDataPackagePresent(oldDataPackageId)){
				errorMessage = "Expected Data Package: "+oldDataPackageId+" is not match with the UI value.";
				packageChange.clickOkConnectedPackageList();
				packageChange.clickBackToHotline();
				return errorMessage;
			}
		}
		
		packageChange.selectConnectedPackageCode(oldDataPackageId);
		packageChange.clickOkConnectedPackageList();
	
		//Fill New Data
		packageChange.clickSelectPackageCode();
		packageChange.selectNewPackage(newDataPackageCode);
		packageChange.clickOkNewPackageList();
		packageChange.clickSelectDealerCode();
		packageChange.selectDealerSalesCode(agentId);
		packageChange.clickOkDealerCodeList();
		packageChange.clickPackageChange();
		
		String popupMsg = packageChange.getPackageChangeSuccessMsg();
		if(!popupMsg.equalsIgnoreCase(StringsPackageChangePage.GSM_PACKAGE_CHANGE_SUCCESS_MSG.value())){
			errorMessage = "Expected Success message: '"+StringsPackageChangePage.GSM_PACKAGE_CHANGE_SUCCESS_MSG.value()+"' is not match with the actual message: '"+popupMsg+"'.";
			packageChange.clickSuccessPopupOk();
			packageChange.clickBackToHotline();
			return errorMessage;
			
		}	
		packageChange.clickSuccessPopupOk();
		packageChange.clickBackToHotline();
		return errorMessage;
	}	
	
	/**Write Error Message into Excel Sheet*/
	public void writeToExcel(String filePath, String sheetName, String mobileNo, String output){
		
		ExcelUtils excelUtils = new ExcelUtils();
		int rowNum = excelUtils.findRow(sheetName, mobileNo);	
		int lastColumnNum = excelUtils.getLastColumnNumber(sheetName);
		
		if(!output.equalsIgnoreCase("")){
			if(output.split(" ")[0].equalsIgnoreCase("Expected")){
				excelUtils.setCellData(filePath, sheetName, "Comment", rowNum, output);
				excelUtils.setCellDataUsingColumnRowNumber(filePath, sheetName, lastColumnNum, rowNum, "FAIL");
				Assert.fail(output);
			}
			else{
				excelUtils.setCellData(filePath, sheetName, "Comment", rowNum, output);
				excelUtils.setCellDataUsingColumnRowNumber(filePath, sheetName, lastColumnNum, rowNum, "NOT_EXECUTED");
			}
		
		}
		else{
			excelUtils.setCellData(filePath, sheetName, "Comment", rowNum, "Updated Successfully");
			excelUtils.setCellDataUsingColumnRowNumber(filePath, sheetName, lastColumnNum, rowNum, "SUCCESS");
		}
	}
	
	//Log into Open Hotline Application
	public void loginToOpenHotline() throws Exception{
		
		openwebdriver = new OpenWebDriver();
		driver = openwebdriver.openCromeBrowser("Hotline");	
		String userName = propFile.getPropertyValue("defaultUser");	
		String passward = propFile.getPropertyValue("defaultPassword");	
		ccbsLoginPage = new CCBSLoginPage(driver);	
		ccbsLoginPage.CCBSLogin(userName, passward);	

	}
	
	//Log into CRM Main Page Application
	public void loginToCRMMainPage() throws Exception{
		
		openwebdriver = new OpenWebDriver();
		driver = openwebdriver.openCromeBrowser("CRMMainPage");
		String userName=propFile.getPropertyValue("defaultUser");	
		String passward=propFile.getPropertyValue("defaultPassword");	
		crmLoginPage= new CRMLoginPage(driver);	
		crmLoginPage.CRMMainLogin(userName, passward);	
	}
	
	
	public void loginToCxRegistrationPage() throws Exception{
		
		openwebdriver = new OpenWebDriver();
		driver = openwebdriver.openCromeBrowser("CxRegistrationPage");
		String userName=propFile.getPropertyValue("defaultUser");	
		String passward=propFile.getPropertyValue("defaultPassword");	
		cxRegPage= new CxRegistrationLoginPage(driver);	
		cxRegPage.loginCxRegistrationPage(userName, passward);	
	}
	
	public void copyResultSheetToResultFolder() throws IOException{
		File source = new File("C:\\bulkAutomatedUpdate\\Bulk_Data_Sheet.xlsx");

		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH-mm-ss");
		String ts = sdf.format(source.lastModified());
		String name = source.getName();
		String ext = name.substring(name.lastIndexOf("."));
		name = name.substring(0, name.lastIndexOf("."));
		String outFileName = name + " " + ts + ext;
		//appending ts to the file name
		System.out.println(" new file name is " + outFileName);

		File destination = new File("C:\\bulkAutomatedUpdate\\Executed_Results_Sheet\\", outFileName);
		FileUtils.copyFile(source,destination);
	}
}
